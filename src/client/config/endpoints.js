export default function ({ BASE_URL }) {
    return {
        PATCH_ENDPOINT: `${BASE_URL}/api/patch/`,
        SOCIAL_AUTH_URL: `${BASE_URL}/api-auth/social/login/`,
        REGISTRATION_ENDPOINT: `${BASE_URL}/api-auth/register/`,
        LOGIN_ENDPOINT: `${BASE_URL}/api-token-auth/`,
        VERIFY_TOKEN_ENDPOINT: `${BASE_URL}/api-token-verify/`,
        REFRESH_TOKEN_ENDPOINT: `${BASE_URL}/api-token-refresh/`,
        LOGOUT_ENDPOINT: `${BASE_URL}/api-auth/logout/`,
        REGISTER_ENDPOINT: `${BASE_URL}/api-auth/register/`,
        PATCH_ENDPOINT_INIT: `${BASE_URL}/api/patch/`,
        PROFILE_ENDPOINT: `${BASE_URL}/api/accounts/users/iam/`,
        UPLOADS_ENDPOINT: `${BASE_URL}/api/accounts/uploads/`,
    };
};
