
export default function revision(state = 0, action = {}) {
    switch (action.type) {
        case 'SET_REVISION':
            return action.rev;
        default:
            return state;
    }
}
