import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import { Form, Alert, Button } from 'react-bootstrap';
import { renderField } from '../../../../helpers/ui';
import { testRequired, email } from '../../../../helpers/validate';


export default class LoginForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        error: PropTypes.string.isRequired,
    };

    render() {
        const {
            handleSubmit,
            error
        } = this.props;

        return (
            <div className="ProfileForm">
                <Form
                    horizontal
                    className="form"
                    role="form"
                    method="post"
                    acceptCharset="UTF-8"
                    onSubmit={handleSubmit(this.props.onSubmit)}
                >
                    {
                        error && (
                            <Alert bsStyle="danger"><strong>{error}</strong></Alert>
                        )
                    }
                    <Field
                        name="email"
                        type="text"
                        component={renderField}
                        placeholder={'Email'}
                        label={'Email'}
                        required
                        validate={[testRequired, email]}
                    />
                    <Field
                        name="password"
                        type="password"
                        component={renderField}
                        placeholder={'Password'}
                        label={'Password'}
                        required
                        validate={testRequired}
                    />
                    <Button type="submit" className="pull-right" bsStyle="success">Login</Button>
                </Form>
            </div>
        );
    }
}
