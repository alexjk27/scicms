import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory, match } from 'react-router';
import Cookies from 'js-cookie';
import { trigger } from 'redial';
import uuid from 'uuid/v1';
import faker from 'faker';
import configureStore from './client/modules/store/configureStore';
import getRoutes from './client/routes';
import './client/routes/styles/font-awesome/css/font-awesome.css';



const initialState = window.__INITIAL_STATE__ || {};

//for test only
const users = {};

require('lodash').range(150).forEach((id) => {
    users[id] = {
        profile: {
            avatar: faker.image.avatar(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            patronymic: faker.name.firstName(),
            email: faker.internet.email(),
            birthday: faker.date.between('1950-12-04', '2000-12-04'),

        },
        scard: {
            scientificDegree: faker.lorem.words(),
            academicRang: faker.lorem.words(),
            officialCapacity: faker.lorem.words(),
            phone: faker.phone.phoneNumberFormat(),
            workPlace: faker.lorem.words(),
            country: faker.address.county(),
            city: faker.address.city(),
            mail: faker.address.streetAddress(),
        },
        cards: [
            {
                id: uuid(),
                userId: id,
                type: 'paper',
                name: faker.commerce.productName(),
                comment: faker.lorem.paragraph(),
                attachments: [
                    {
                        label: 'Paper',
                        url: faker.image.imageUrl(),
                        fileId: 'attach_1',
                    }
                ],
                desk: null,
                beforePosition: id - 1,
                position: id,
                afterPosition: id + 1,
            },
            {
                id: uuid(),
                userId: id,
                type: 'article',
                name: faker.commerce.productName(),
                comment: faker.lorem.paragraph(),
                attachments: [
                    {
                        label: 'Article',
                        url: faker.image.imageUrl(),
                        fileId: 'attach_1',
                    },
                    {
                        label: 'Authors',
                        url: faker.image.imageUrl(),
                        fileId: 'attach_2',
                    },
                ],
                desk: null,
                beforePosition: id - 1,
                position: id,
                afterPosition: id + 1,
            },
            {
                id: uuid(),
                userId: id,
                type: 'booking',
                name: faker.commerce.productName(),
                options: [
                    {
                        id: 1,
                        label: faker.lorem.words(),
                    },
                    {
                        id: 2,
                        label: faker.lorem.words(),
                    },
                    {
                        id: 3,
                        label: faker.lorem.words(),
                    },
                ],
                comment: faker.lorem.paragraph(),
                desk: null,
                beforePosition: id - 1,
                position: id,
                afterPosition: id + 1,
            },
            {
                id: uuid(),
                userId: id,
                name: faker.commerce.productName(),
                type: 'equals',
                options: [
                    {
                        id: 1,
                        label: faker.lorem.words(),
                    },
                    {
                        id: 2,
                        label: faker.lorem.words(),
                    },
                    {
                        id: 3,
                        label: faker.lorem.words(),
                    },
                ],
                comment: faker.lorem.paragraph(),
                desk: null,
                beforePosition: id - 1,
                position: id,
                afterPosition: id + 1,
            },
            {
                id: uuid(),
                userId: id,
                name: faker.commerce.productName(),
                type: 'billing',
                comment: faker.lorem.paragraph(),
                paymentMethod: faker.commerce.productName(),
                options: [
                    {
                        id: 1,
                        cost: 1,
                        label: faker.lorem.words(),
                    },
                    {
                        id: 2,
                        cost: 2,
                        label: faker.lorem.words(),
                    },
                    {
                        id: 3,
                        cost: 3,
                        label: faker.lorem.words(),
                    },
                ],
                desk: null,
                beforePosition: id - 1,
                position: id,
                afterPosition: id + 1,
            },
        ]
    };
});

initialState.users = users;

const store = configureStore(initialState);
const routes = getRoutes(store);


window.gettext = v => v;


const callTrigger = (renderProps) => {
    let promise = Promise.resolve();

    if (!renderProps) {
        return promise;
    }

    const { components } = renderProps;
    // Define locals to be provided to all lifecycle hooks:
    const locals = {
        path: renderProps.location.pathname,
        query: renderProps.location.query,
        params: renderProps.params,
        dispatch: store.dispatch,
        getState: store.getState,
        token: Cookies.get('jwt'),
    };

    // Don't fetch data for initial route, server has already done the work:
    if (window.INITIAL_STATE) {
        // Delete initial data so that subsequent data fetches can occur:
        delete window.INITIAL_STATE;
    } else {
        // Fetch mandatory data dependencies for 2nd route change onwards:
        promise = trigger('fetch', components, locals);
    }

    // Fetch deferred, client-only data dependencies:
    return promise.then(() => trigger('defer', components, locals));
};


const render = () => {
    const { pathname, search, hash } = window.location;
    const location = `${pathname}${search}${hash}`;

    // Pull child routes using match. Adjust Router for vanilla webpack HMR,
    // in development using a new key every time there is an edit.
    match({ routes, location }, (error, redirectLocation, renderProps) => {
        // Render app with Redux and router context to container element.
        // We need to have a random in development because of `match`'s dependency on
        // `routes.` Normally, we would want just one file from which we require `routes` from.
        callTrigger(renderProps).then(() => {
            ReactDOM.render(
                <Provider store={store} key={Math.random()}>
                    <Router history={browserHistory} routes={routes} key={Math.random()} />
                </Provider>,
                document.getElementById('root')
            );
        });
    });
};

// Listen for route changes on the browser history instance:
browserHistory.listen((location) => {
    // Match routes based on location object:
    match({ routes, location }, (error, redirectLocation, renderProps) => {
        // Get array of route handler components:
        if (!renderProps) return;
        callTrigger(renderProps);
    });
});

render();

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept();
}
