import React, { Component, PropTypes } from 'react';
import { Nav, Navbar, NavItem, NavDropdown, MenuItem, Overlay, Popover } from 'react-bootstrap';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as actions from '../../../../../modules/actions';
import { toCamelCaseObject } from '../../../../../helpers/parsers';
import EmailSignInForm from '../EmailSignInForm';
import SocialOAuth from '../SocialOAuth';


class Header extends Component {
    static propTypes = {
        auth: PropTypes.object,
        logout: PropTypes.func.isRequired,
        router: PropTypes.object.isRequired,
    };

    state = {
        menuOpen: false,
        show: false,
        target: null,
    };

    onClickLogIn = (e) => {
        this.setState({
            target: e.target,
            show: !this.state.show,
        });
    };

    onSelect = (key) => {
        const { router, logout } = this.props;
        if (key === 'logout') {
            logout();
        } else if (!key.startsWith('#')) {
            router.push(key);
        }
    };

    renderProfileNav = () => {
        const { auth: { profile } } = this.props;
        const data = toCamelCaseObject({ ...profile });
        const { lastName, firstName } = data;
        data.fullName = (lastName && firstName) ? `${lastName} ${firstName}` : data.email;

        return (
            <NavDropdown onSelect={this.onSelect} eventKey={5} title={data.fullName} id="profile-nav-dropdown">
                <MenuItem eventKey={'/profile/'}>Profile</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey={'logout'}>Logout</MenuItem>
            </NavDropdown>
        );
    };

    render() {
        const { auth: { token } } = this.props;

        return (
            <div className="Header">
                <Overlay
                    show={this.state.show}
                    target={this.state.target}
                    placement="bottom"
                    containerPadding={20}
                    onHide={() => this.setState({ show: false })}
                    rootClose
                >
                    <Popover id="popover-contained">
                        <EmailSignInForm onSuccess={() => this.setState({ show: false })} />
                        <div>
                            <div className="or">~~ or ~~</div>
                            <SocialOAuth provider="google" onLogin={() => this.setState({ show: false })}>
                                Sign In with Google
                            </SocialOAuth>
                            <SocialOAuth provider="facebook" onLogin={() => this.setState({ show: false })}>
                                Sign In with facebook
                            </SocialOAuth>
                            <SocialOAuth provider="vk" onLogin={() => this.setState({ show: false })}>
                                Sign In with vk
                            </SocialOAuth>
                        </div>
                    </Popover>
                </Overlay>

                <Navbar inverse fixedTop>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="/">React-Bootstrap</a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight onSelect={this.onSelect}>
                            {
                                token ? (
                                    this.renderProfileNav()
                                ) : (
                                    [
                                        <NavItem key="1" eventKey={'/registration/'}>Sign Up</NavItem>,
                                        <NavItem
                                            key="#2"
                                            eventKey={4}
                                            className={classnames({ open: this.state.show })}
                                            onClick={this.onClickLogIn}
                                        >Sign In</NavItem>
                                    ]

                                )
                            }
                        </Nav>

                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

export default connect(
    state => ({
        auth: state.auth,
    }),
    dispatch => ({
        logout: () => dispatch(actions.logout()),
    }),
)(withRouter(Header));
