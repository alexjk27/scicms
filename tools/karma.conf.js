const webpackConfig = require('./webpack.dev.config.js');
const path = require('path');


webpackConfig.entry.main = path.join(__dirname, '../src/client.js');


module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['mocha', 'chai', 'sinon'],
        files: [
            '../tests/**/*.spec.js'
        ],
        exclude: [
            'node_modules'
        ],
        preprocessors: {
            '../tests/**/*.spec.js': ['webpack', 'sourcemap']
        },
        // webpack configuration
        webpack: webpackConfig,
        webpackMiddleware: {
            stats: 'errors-only'
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['ChromeWithoutSecurity'],
        customLaunchers: {
            ChromeWithoutSecurity: {
                base: 'Chrome',
                //flags: ['--disable-web-security']
            }
        },
        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,
        concurrency: Infinity
    });
};
