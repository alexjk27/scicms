import React, { Component, PropTypes } from 'react';
import { Panel } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../modules/actions';
import PaperForm from '../common/components/PaperForm';


class EditPaperPage extends Component {

    static propTypes = {
        setEntity: PropTypes.func.isRequired,
    };

    onSubmit = (values) => {
        this.props.setEntity(values);
    };

    render() {
        return (
            <Panel className="EditPaperPage">
                <PaperForm onSubmit={this.onSubmit} {...this.props} />
            </Panel>
        );
    }
}

export default connect(
    state => ({
        sections: state.conference.sections,
        presentationTypes: state.conference.presentationTypes,
        paperFields: state.conference.paperFields,
        initialValues: (state.users[state.auth.profile.email] || {}).paper || {},
    }),
    dispatch => ({
        setEntity: values => dispatch(actions.setEntity('paper', values)),
    }),
)(
    reduxForm({
        form: 'PaperForm',
        fields: ['paper'],
    })(EditPaperPage)
);
