import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import { IntlProvider, addLocaleData } from 'react-intl';
import { provideHooks } from 'redial';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import * as actions from '../../../modules/actions';
import config from '../../../config';
import { submit } from '../../../modules/services/http';
import Header from './containers/Header';
import * as i18n from '../../../i18n';
import './App.less';


const locale = navigator.language.substr(0, 2).toLowerCase() || 'en';
const canUseDOM = typeof window !== 'undefined';


addLocaleData({
    locale: 'ru',
    parentLocale: 'en'
});
moment.locale(locale);


@connect()
class App extends Component {
    static propTypes = {
        location: React.PropTypes.object,
        children: React.PropTypes.object,
        dispatch: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            viewport: canUseDOM ?
            { width: window.innerWidth, height: window.innerHeight } :
            { width: 1366, height: 768 } // Default size for server-side rendering
        };
        this.props.dispatch(actions.setScreenSize(this.state.viewport));
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
        window.addEventListener('orientationchange', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
        window.removeEventListener('orientationchange', this.handleResize);
    }

    handleResize = () => {
        const viewport = { width: window.innerWidth, height: window.innerHeight };
        if (this.state.viewport.width !== viewport.width ||
            this.state.viewport.height !== viewport.height) {
            this.setState({ viewport });
            this.props.dispatch(actions.setScreenSize(viewport));
        }
    };

    render() {
        return (
            <IntlProvider locale={locale} messages={i18n[locale]}>
                <div className="App">
                    <Header />
                    <ReactCSSTransitionGroup
                        className="App-content"
                        component="div"
                        transitionName="children"
                        transitionEnterTimeout={300}
                        transitionLeaveTimeout={300}
                    >
                        {React.cloneElement(this.props.children, {
                            key: this.props.location.pathname
                        })}
                    </ReactCSSTransitionGroup>
                </div>
            </IntlProvider>
        );
    }

}


export default provideHooks({
    fetch: ({ dispatch, token, getState }) => {
        const state = getState();
        if (token && _.isEmpty(state.auth.profile)) {
            return submit(config.REFRESH_TOKEN_ENDPOINT, { token })(token).then((data) => {
                const paths = [
                    '/counter',
                    '/users'
                ];
                dispatch(actions.setToken(data.token));
                return dispatch(actions.setProfileData()).then(() => {
                    return dispatch(actions.loadPatch(paths));
                });
            }).catch(() => {
                dispatch(actions.authFailed());
            });
        } else {
            // anonymous user
            const paths = [
                '/counter'
            ];
            return dispatch(actions.loadPatch(paths));
        }
    }
})(App);
