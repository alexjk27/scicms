import _ from 'lodash';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import jsonpatch from 'fast-json-patch';
import { APPLY_PATCH } from '../actions';
import * as reducers from '../reducers';
import * as middlware from '../middlware';


const rootReducer = (role, state, action, ...rest) => {
    const fReducers = _.pickBy(reducers, (value, key) => (role === 'participant' ? key !== 'participants' : true));
    const combinedReducers = combineReducers({
        form: formReducer,
        ...fReducers,
    });

    if (action.error && action.prevState) {
        return action.prevState;
    } else if (action.type === APPLY_PATCH) {
        const nextState = { ...state };
        const splitedPatch = _.groupBy(action.patch, p => p.path.split('/')[1]);

        Object.keys(splitedPatch).forEach((reducer) => {
            if (_.isObject(state[reducer])) {
                nextState[reducer] = { ...state[reducer] };
            }

            try {
                jsonpatch.apply(nextState, splitedPatch[reducer], true);
            } catch (err) {
                console.error(err);
                throw err;
            }
        });
        return nextState;
    } else {
        return combinedReducers(state, action, ...rest);
    }
};


const finalCreateStore = compose(
    applyMiddleware(thunk, ...(_.values(middlware)))
)(createStore);


export default function configureStore(initialState = {}, role = 'participant') {
    let store;

    if (typeof window !== 'undefined') {
        store = finalCreateStore(
            (state, action, ...rest) => rootReducer(role, state, action, ...rest),
            initialState,
            window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
        );
    } else {
        store = finalCreateStore(
            (state, action, ...rest) => rootReducer(role, state, action, ...rest),
            initialState
        );
    }

    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(require('../reducers').default) // eslint-disable-line global-require
        );
    }

    return store;
}
