import React, { PropTypes, Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as actions from '../../../../../modules/actions';
import config from '../../../../../config';


const providerAlias = {
    google: 'google-oauth2',
    vk: 'vk-oauth2',
    facebook: 'facebook'
};


@connect()
class SocialOAuth extends Component {

    static propTypes = {
        provider: PropTypes.string.isRequired,
        children: PropTypes.any.isRequired,
        onLogin: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired,
    };


    onOpenPopUp = (e) => {
        const url = `${config.SOCIAL_AUTH_URL}${providerAlias[this.props.provider]}`;

        window.open(url, '_blank', 'toolbar=no,scrollbars=yes,resizable=yes,top=500,left=500,width=640,height=400');
        window.onGetCode = async (token) => {
            const { dispatch } = this.props;
            dispatch(actions.setToken(token));
            await dispatch(actions.setProfileData());
            this.props.onLogin(token);
        };
        e.preventDefault();
    };

    render() {
        const { children, provider } = this.props;
        return (
            <button className={`btn btn-block btn-social btn-${provider}`} onClick={this.onOpenPopUp}>
                <i className={`fa fa-${provider}`} />
                {children}
            </button>
        );
    }
}

export default withRouter(SocialOAuth);
