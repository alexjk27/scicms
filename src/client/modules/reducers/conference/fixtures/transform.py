import json

j = json.loads(open('participant.json').read())
bj = [i for i in j if 'i18n_source' not in i['fields']]
tr = {'%s_%s' % (i['model'], i['fields']['i18n_source']): i['fields'] for i in j if 'i18n_source' in i['fields']}

results = {}
for k in bj:
    group = results.setdefault(k['model'].split('.')[1], [])
    t = tr['%si18n_%s' % (k['model'], k['pk'])]
    data = {
        'label': k['fields'].get('degree', k['fields'].get('rang')),
        'position': k['fields'].get('weight'),
        'translations': {
            t['i18n_language']: t.get('degree', t.get('rang'))
        }
    }
    group.append(data)

json.dump(results, open('res.json', 'w+'))
#print(results)
