import React, { Component, PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap';
import ConferenceHeader from '../common/components/ConferenceHeader';
import ProfileMenu from '../common/components/ProfileMenu';


class ProfilePage extends Component {
    static propTypes = {
        children: PropTypes.object.isRequired
    };

    render() {
        return (
            <div className="ProfilePage">
                <ConferenceHeader />
                <div className="container">
                    <Row>
                        <Col md={3}>
                            <ProfileMenu />
                        </Col>
                        <Col md={9}>
                            {this.props.children}
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}


export default ProfilePage;
