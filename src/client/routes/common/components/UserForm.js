import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';


const UserForm = (props) => {
    const { handleSubmit, pristine, reset, submitting } = props;

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="idFirstName">First Name</label>
                <div>
                    <Field id="idFirstName" name="firstName" component="input" type="text" placeholder="First Name" />
                </div>
            </div>
            <div>
                <label htmlFor="idLastName">Last Name</label>
                <div>
                    <Field id="idLastName" name="lastName" component="input" type="text" placeholder="Last Name" />
                </div>
            </div>
            <div>
                <button type="submit" disabled={pristine || submitting}>Submit</button>
                <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</button>
            </div>
        </form>
    );
};

UserForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    reset: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
};


export default reduxForm({
    form: 'userForm'
})(UserForm);
