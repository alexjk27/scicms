import * as actionTypes from '../actions';


const initialState = {
};


export default function patches(state = initialState, action = {}) {
    const { type, path, page, page_size: pageSize } = action;

    switch (type) {
        case actionTypes.SET_PATCH_PAGINATION:
            return { ...state, [path]: { path, page, pageSize } };
        default:
            return actionTypes.reduceTriggeredAction(state, action, [actionTypes.SET_PATCH_PAGINATION]);
    }
}
