var _ = require('lodash');
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
//var CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = {
    context: path.resolve(__dirname, 'src'),
    devtool: 'eval',

    entry: {
        main: [
            'react-hot-loader/patch',
            // activate HMR for React

            'webpack-dev-server/client?http://localhost:8000',
            // bundle the client for webpack-dev-server
            // and connect to the provided endpoint

            'webpack/hot/only-dev-server',

            path.join(__dirname, '../src/client.js')
        ]
    },

    output: {
        path: path.join(__dirname, '../dist/static/'),
        filename: 'js/[name].js',
        publicPath: '/static/'
    },

    devServer: {
        hot: true,
        // activate hot reloading

        contentBase: path.resolve(__dirname, '../src/client/public/html'),
        // match the output path

        publicPath: '/static/',
        // match the output `publicPath`
        headers: { 'Access-Control-Allow-Origin': '*' },
        proxy: {
            '/api': {
                target: 'http://0.0.0.0:8082',
                secure: false
            },
        }
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new ExtractTextPlugin('css/[name].css')
    ],

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader', // 'babel-loader' is also a legal name to reference
                options: {
                    plugins: ['transform-decorators-legacy', 'transform-runtime', 'react-hot-loader/babel'],
                    presets: [['es2015', { 'modules': false }], 'stage-0', 'react'],
                    cacheDirectory: '/tmp/'
                },
                exclude: /(node_modules|bower_components)/
            },
            {
                test: /\.json$/, loader: 'json-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    {
                        fallbackLoader: 'style-loader',
                        loader: 'css-loader'
                    }
                )
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract(
                    {
                        fallbackLoader: 'style-loader',
                        loader: ['css-loader', 'less-loader']
                    }
                )
            },
            {
                test: /\.(png|gif|jpg|eot|ttf|svg)$/,
                loader: 'file-loader',
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
        ]
    }
};

