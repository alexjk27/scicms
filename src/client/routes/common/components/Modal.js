import React, { PropTypes, Component } from 'react';
import { Modal as BSModal } from 'react-bootstrap';
import { withRouter } from 'react-router';


@withRouter
export default class Modal extends Component {
    static propTypes = {
        title: PropTypes.any.isRequired,
        modalName: PropTypes.string.isRequired,
        children: PropTypes.object.isRequired,
        router: PropTypes.object.isRequired,
        className: PropTypes.any,
        onClose: PropTypes.func,
    };

    onClose = (e) => {
        const { modalName, router: { location, replace } } = this.props;
        if (this.props.onClose) {
            this.props.onClose(e);
        }
        const { query } = location;
        delete query[modalName];

        replace({
            ...location,
            query
        });
    };

    render() {
        const { modalName, title, children, className, router: { location } } = this.props;

        return (
            <BSModal
                className={className}
                show={location.query[modalName] === 'on'}
                onHide={this.onClose}
            >
                <BSModal.Header closeButton>
                    <BSModal.Title>{title}</BSModal.Title>
                </BSModal.Header>
                <BSModal.Body>
                    {children}
                </BSModal.Body>
            </BSModal>
        );
    }
}
