export const INCREMENT = 'INCREMENT';
export function increment() {
    return {
        type: INCREMENT,
        serverSync: true,
    };
}

export const DECREMENT = 'DECREMENT';
export function decrement() {
    return {
        type: DECREMENT,
        serverSync: true,
    };
}
