import { get } from '../services/http';
import * as actions from '../actions';
import config from '../../config';


const paginatedPatch = ({ dispatch, getState }) => next => (action) => {
    const { type, isPaginated, ...params } = action;

    if (!isPaginated) {
        return next(action);
    }

    const prevState = getState();
    const { auth: { token } } = prevState;

    dispatch({ type, ...params });

    dispatch({
        ...params,
        type: actions.triggered(type),
    });

    return get(`${config.PATCH_ENDPOINT}paginated/${prevState.conference.id}/`, params)(token).then((data) => {
        const { patch, ...rest } = data;
        const result = next({
            data: { [params.path]: { ...params, ...rest } },
            type: actions.succeeded(type),
        });
        dispatch(actions.applyPatch(patch));
        return result;
    }).catch((error) => {
        dispatch({
            ...params,
            prevState,
            type: actions.failed(type),
            error: {
                ...error.response,
                statusCode: error.response && error.response.status,
            },
        });
    });
};

export default paginatedPatch;
