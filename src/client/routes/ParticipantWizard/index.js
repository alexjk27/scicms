import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as actions from '../../modules/actions';
import Wizard from '../common/components/Wizard';
import WizardPage from '../common/components/Wizard/WizardPage';
import PaperForm from '../common/components/PaperForm';
import ProfileForm from '../common/components/ProfileForm';
import SCardForm from '../common/components/SCardForm';
import { toCamelCaseObject } from '../../helpers/parsers';
import './ParticipantWizard.less';


const stateToProps = state => ({
    //form: state.form.ParticipantWizard,
    entities: (state.users[state.auth.profile.email] || {}),
    auth: state.auth,
    scientificDegree: state.conference.scientificDegree,
    academicRang: state.conference.academicRang,
    sections: state.conference.sections,
    presentationTypes: state.conference.presentationTypes,
    paperFields: state.conference.paperFields,
});


@connect(stateToProps)
@withRouter
export default class ParticipantWizard extends Component {
    static propTypes = {
        entities: PropTypes.object.isRequired,
        auth: PropTypes.object.isRequired,
        scientificDegree: PropTypes.array.isRequired,
        academicRang: PropTypes.array.isRequired,
        sections: PropTypes.array.isRequired,
        presentationTypes: PropTypes.array.isRequired,
        paperFields: PropTypes.array.isRequired,
        dispatch: PropTypes.func.isRequired,
        router: PropTypes.object.isRequired,
    };

    onSuccess = (batch) => {
        this.props.dispatch(actions.setBatchEntity(batch)).then(() => {
            this.props.router.push('/profile/');
        });
    };

    render() {
        const {
            entities: {
                profile,
                paper,
                sCard,
            },
            auth,
            scientificDegree,
            academicRang,
            sections,
            presentationTypes,
            paperFields
        } = this.props;

        const initialValues = toCamelCaseObject({
            ...(profile || auth),
            ...paper,
            ...sCard,
        });

        return (
            <div className="ParticipantWizard clearfix">
                <Wizard form="ParticipantWizard" initialValues={initialValues} onSuccess={this.onSuccess}>
                    <WizardPage
                        name="profile"
                        title="Profile"
                        component={ProfileForm}
                    />
                    <WizardPage
                        {...{ scientificDegree, academicRang }}
                        name="sCard"
                        title="Speaker card"
                        component={SCardForm}
                    />
                    <WizardPage
                        {...{ sections, presentationTypes, paperFields }}
                        name="paper"
                        title="Paper"
                        component={PaperForm}
                    />
                </Wizard>
            </div>
        );
    }
}
