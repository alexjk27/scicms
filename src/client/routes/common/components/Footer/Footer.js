import React, { PropTypes, Component } from 'react';
import './Footer.less';


export default class Footer extends Component {
    static propTypes = {
        text: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <footer className="Footer">
                <div className="container">
                    <p className="text-muted">© 2015 SciCms.com</p>
                </div>
            </footer>
        );
    }
}
