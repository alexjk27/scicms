import React, { Component, PropTypes } from 'react';
import { Panel } from 'react-bootstrap';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from '../../modules/actions';
import ProfileForm from '../common/components/ProfileForm';
import ConferenceHeader from '../common/components/ConferenceHeader';


class EditParticipantProfilePage extends Component {
    static propTypes = {
        registration: PropTypes.func.isRequired,
        participantId: PropTypes.string.isRequired,
    };

    onSubmit = (values) => {
        this.props.registration(this.props.participantId, values);
    };

    render() {
        return (
            <div className="EditParticipantProfilePage">
                <ConferenceHeader />
                <div className="container">
                    <Panel>
                        <ProfileForm onSubmit={this.onSubmit} {...this.props} />
                    </Panel>
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        scientificDegree: state.conference.scientificDegree,
        academicRang: state.conference.academicRang,
        participants: state.participants,
    }),
    dispatch => ({
        registration: (id, values) => dispatch(actions.setParticipantEntity(id, 'profile', values)),
    }),
    (stateProps, dispatchProps, ownProps) => {
        const participantId = ownProps.params.id;
        const initialValues = stateProps.participants[participantId].entities.profile;

        return { ...stateProps, ...dispatchProps, ...ownProps, initialValues, participantId };
    }
)(
    reduxForm({
        form: 'EditParticipantProfilePage',
    })(EditParticipantProfilePage)
);
