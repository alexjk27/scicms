import React, { Component } from 'react';
import { Panel, InputGroup, Button, FormControl, ListGroup, ListGroupItem } from 'react-bootstrap';


class SideBar extends Component {

    renderSearch = () => {
        return (
            <InputGroup>
                <FormControl type="text" />
                <InputGroup.Button>
                    <Button><i className="fa fa-search" /></Button>
                </InputGroup.Button>
            </InputGroup>
        );
    };

    render() {
        return (
            <Panel className="SideBar" header={this.renderSearch()}>
                <ListGroup fill>
                    <ListGroupItem>Item 1</ListGroupItem>
                    <ListGroupItem>Item 2</ListGroupItem>
                    <ListGroupItem>&hellip;</ListGroupItem>
                </ListGroup>
            </Panel>
        );
    }
}


export default SideBar;
