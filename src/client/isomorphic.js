import React from 'react';
import Express from 'express';
import { match, RouterContext } from 'react-router';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import routes from './routes';
import configureStore from './store/configureStore';
import getDbStore from '../api/store';


const router = Express.Router();


const HOST = process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:8082' : '';
const EXTRA_SCRIPT = process.env.NODE_ENV === 'development' ?
    'http://localhost:8082/webpack-dev-server.js' :
    '';


const errorsParser = (error, res) => {
    console.trace(error.message);
    console.error(error.stack.split('\n'));

    if (error.response && error.response.status) {
        console.trace(error.response);
        if (error.response.status === 403) {
            res.clearCookie('jwt');
            res.redirect('/');
        }
        const { status, statusText, config: { method, url, data } } = error.response;
        const errorHTML = 'API error!<br>' +
            `Request: ${method} ${url}, DATA (${JSON.stringify(data)})<br>` +
            `Response: ${statusText}<br>`;
        res.status(status).send(errorHTML);
    } else {
        res.status(500).send(error.stack);
    }
};


const prepareData = (title, html, preloadedState) => {
    return {
        title, html, preloadedState, EXTRA_SCRIPT, HOST
    };
};


router.get('/', (req, res) => {
    match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
        if (error) {
            res.status(500).send(error.message);
        } else if (redirectLocation) {
            res.redirect(302, redirectLocation.pathname + redirectLocation.search);
        } else if (renderProps) {
            const title = 'SciCms';

            getDbStore('conference', 'user1').then((dbStore) => {
                return dbStore.getState();
            }).then((initialState) => {
                console.log(initialState);
                const store = configureStore(initialState || {});
                const html = renderToString(
                    <Provider store={store}>
                        <RouterContext {...renderProps} />
                    </Provider>
                );
                res.status(200).render('index', prepareData(title, html, JSON.stringify(initialState || {})));
            }).catch((err) => {
                errorsParser(err, res);
            });
        } else {
            res.status(404).send('Not found');
        }
    });
});


export default router;
