import React, { Component, PropTypes } from 'react';
import { Panel } from 'react-bootstrap';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from '../../modules/actions';
import SCardForm from '../common/components/SCardForm';
import { toCamelCaseObject } from '../../helpers/parsers';
import './EditSCardPage.less';


class EditSCardPage extends Component {
    static propTypes = {
        setEntity: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className="SCardForm">
                <Panel>
                    <SCardForm onSubmit={this.props.setEntity} {...this.props} />
                </Panel>
            </div>
        );
    }
}


export default connect(
    state => ({
        scientificDegree: state.conference.scientificDegree,
        academicRang: state.conference.academicRang,
        initialValues: (
            toCamelCaseObject((state.users[state.auth.profile.email] || {}).profile || state.auth.profile)
        ),
    }),
    dispatch => ({
        setEntity: values => dispatch(actions.setEntity('profile', values)),
    }),
)(
    reduxForm({
        form: 'EditSCardPage',
    })(EditSCardPage)
);
