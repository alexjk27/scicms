import _ from 'lodash';
import assert from 'assert';
import configureStore from '../../src/client/modules/store/configureStore';
import { splitEntities } from '../../src/client/helpers/utils';


describe('Utils', () => {
    describe('splitEntities', () => {
        it('should return -1 when the value is not present', () => {
            const users = {};
            _.range(5).forEach((id) => {
                users[id] = {
                    paper: {
                        name: `name ${id}`,
                        desc: null,
                        position: id,
                    }
                };
            });
            const store = configureStore({ users });
            const { users: list } = store.getState();
            const result = splitEntities(list, 'paper');
            assert.equal(result.default.length, 5);
            //console.log();
        });
    });
});
