export const SET_CARD = 'SET_CARD';
export function setEntity(name, values, username) {
    return (dispatch, getState) => {
        return dispatch({
            type: SET_CARD,
            serverSync: true,
            name,
            values,
            username: username || getState().auth.profile.email
        });
    };
}


export const SET_BATCH_ENTITY = 'SET_BATCH_ENTITY';
export function setBatchEntity(batch, username) {
    return (dispatch, getState) => {
        return dispatch({
            type: SET_BATCH_ENTITY,
            serverSync: true,
            batch,
            username: username || getState().auth.profile.email
        });
    };
}
