import * as actions from '../actions';



export default function apiMiddleware({ dispatch, getState }) {
    return next => (action) => {
        const { api, type, onTriggered, onSuccess, onFailure, ...rest } = action;

        if (!api) {
            return next(action);
        }

        if (onTriggered) {
            onTriggered(dispatch, getState, ...rest);
        } else {
            dispatch({
                type: actions.triggered(type),
                ...rest,
            });
        }
        const { auth: { token } } = getState();

        return api(token)
            .then((result) => {
                if (onSuccess) {
                    return onSuccess({ result, dispatch, getState, type: actions.succeeded(type), ...rest });
                } else {
                    return dispatch({
                        type: actions.succeeded(type),
                        result,
                        ...rest,
                    });
                }
            })
            .catch((error) => {
                if (onFailure) {
                    onFailure({ error, dispatch, getState, type: actions.failed(type), ...rest });
                } else {
                    dispatch({
                        type: actions.failed(type),
                        error: error.message,
                        ...rest,
                    });
                }
                throw error;
            });
    };
}
