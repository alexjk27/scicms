export const SET_PATCH_PAGINATION = 'SET_PATCH_PAGINATION';
export function loadPaginatedPatch(path, page, pageSize, userId) {
    return {
        type: SET_PATCH_PAGINATION,
        isPaginated: true,
        orderBy: ['asc', 'id'],
        pageSize,
        userId,
        path,
        page,
    };
}
