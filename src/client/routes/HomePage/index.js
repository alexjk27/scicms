import React, { Component } from 'react';
import ConferenceHeader from '../common/components/ConferenceHeader';


class HomePage extends Component {

    render() {
        return (
            <div className="HomePage">
                <ConferenceHeader />
                <div className="container">
                    <h1 className="text-center">HomePage</h1>
                    <input type="checkbox" />
                </div>
            </div>
        );
    }
}


export default HomePage;
