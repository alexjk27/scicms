import * as actions from '../actions';


const initialState = {
};


export default function users(state = initialState, action = {}) {
    switch (action.type) {
        case actions.SET_CARD: {
            const { username } = action;
            const card = { [action.name]: action.values };
            return { ...state, [username]: { ...state[username], ...card } };
        }
        case actions.SET_BATCH_ENTITY: {
            const { username } = action;
            return { ...state, [username]: action.batch };
        }
        case actions.MOVE_CARD: {
            const { userId, cardId, position } = action;
            return {
                ...state,
                [userId]: {
                    ...state[userId],
                    cards: state[userId].cards.map(card => (cardId === card.id ? { ...card, ...position } : card)),
                }
            };
        }
        default:
            return actions.reduceTriggeredAction(state, action, [actions.SET_CARD, actions.SET_BATCH_ENTITY]);
    }
}
