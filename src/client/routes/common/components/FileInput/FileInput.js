import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import appConfig from '../../../../config';
import * as actions from '../../../../modules/actions';
import './FileField.less';


class FileInput extends Component {

    static propTypes = {
        value: PropTypes.any,
        uploads: PropTypes.object,
        placeholder: PropTypes.string,
        upload: PropTypes.func.isRequired,
        fieldId: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    };

    handleFileChange = (e) => {
        if (e.target.files) {
            const files = e.target.files;
            this.props.upload(this.props.fieldId, files).then(({ file }) => {
                this.props.onChange(file);
            });
        }
    };

    handleFileRemove = (file) => {
        console.log(file);
    };

    render() {
        const { placeholder, value, uploads, fieldId } = this.props;
        const progress = uploads.progress[fieldId] || 0;
        const fileName = value ? value.file.split('/').slice(-1) : '';
        const canDownload = !!value;

        return (
            <div className="FileUpload">
                <div className="input-group">
                    <input
                        type="text"
                        placeholder={placeholder}
                        readOnly
                        value={fileName}
                        className="form-control readonly"
                    />
                    <div className="input-group-btn">
                        <a
                            href={`${appConfig.BASE_URL}/static/${value.file}`}
                            title={'Download'}
                            onClick={this.handleFileRemove}
                            className={`btn btn-success ${canDownload ? '' : 'hidden'}`}
                            type="button"
                        >
                            <i className="glyphicon glyphicon-download-alt" />
                        </a>
                        <button className="btn btn-default" type="button">
                            <input
                                onChange={this.handleFileChange}
                                type="file" className="FileUpload-input"
                            />
                            <i className="glyphicon glyphicon-folder-open" /> {'Browse'}
                        </button>
                    </div>
                </div>
                <div className={`progress ${progress > 0 && progress < 100 ? '' : 'hidden'}`}>
                    <div
                        className="progress-bar progress-bar-success progress-bar-striped"
                        role="progressbar"
                        style={{ width: `${progress}%` }}
                    >
                        {progress}%
                        <span className="sr-only">{progress}% {'Complete (success)'}</span>
                    </div>
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        uploads: state.uploads,
    }),
    dispatch => ({
        upload: (fieldId, files) => dispatch(actions.uploadFile(fieldId, files)),
    }),
)(FileInput);
