export const SET_SCREEN = 'SET_SCREEN';

export function setScreenSize({width, height}) {
    return (dispatch) => {
        dispatch({ type: SET_SCREEN, width, height });
    };
}
