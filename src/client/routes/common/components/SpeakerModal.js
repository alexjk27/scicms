import React, { Component, PropTypes } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import Modal from '../components/Modal';
import EditProfileModal from './EditProfileModal';
import EditSCardModal from './EditSCardModal';


export default class SpeakerModal extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        modalName: PropTypes.string.isRequired,
    };

    render() {
        const { title, modalName } = this.props;
        return (
            <Modal modalName={modalName} title={title}>
                <Tabs bsStyle="tabs" id="idSpeakerModal">
                    <Tab eventKey="1" title="Profile"><EditProfileModal /></Tab>
                    <Tab eventKey="2" title="sCard"><EditSCardModal /></Tab>
                </Tabs>
            </Modal>
        );
    }
}
