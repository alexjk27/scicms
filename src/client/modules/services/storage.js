import Cookies from 'js-cookie';


export default {
    setItem: (key, val) => new Promise((resolve) => {
        Cookies.set(key, val, { path: '/' });
        resolve();
    }),
    getItem: key => new Promise((resolve) => {
        resolve(Cookies.get(key));
    }),
    removeItem: key => new Promise((resolve) => {
        resolve(Cookies.remove(key));
    }),
};
