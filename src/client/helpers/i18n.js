import _ from 'lodash';


export function tr(context, name, language) {
    if (!context.translations || !_.isObject(context.translations)) {
        throw new Error('Wrong translation context');
    }
    const translate = context.translations[language] && context.translations[language][name];
    return translate || context[name];
}
