import React, { Component, PropTypes } from 'react';
import { Panel } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as actions from '../../modules/actions';
import LoginForm from '../common/components/LoginForm';
import ConferenceHeader from '../common/components/ConferenceHeader';


@connect()
@reduxForm({ form: 'LoginPage' })
@withRouter
export default class LoginPage extends Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        router: PropTypes.object.isRequired,
    };

    onSubmit = (values) => {
        return this.props.dispatch(actions.login(values)).then(() => {
            this.props.router.push('/profile/');
        });
    };

    render() {
        return (
            <div className="EditPaperPage">
                <ConferenceHeader />
                <div className="container">
                    <Panel>
                        <LoginForm onSubmit={this.onSubmit} {...this.props} />
                    </Panel>
                </div>
            </div>
        );
    }
}
