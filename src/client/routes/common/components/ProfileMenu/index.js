import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { withRouter } from 'react-router';
import './ProfileMenu.less';


const getIconClass = (url) => {
    return ({
        '/profile/': 'fa fa-user',
        '/papers/': 'fa fa-desktop',
        // articles: (<span><i className="fa fa-book"></i> {label}</span>),
        // bookings: (<span><i className="fa fa-home"></i> {label}</span>),
        // equals: (<span><i className="fa fa-check"></i> {label}</span>),
        // receipts: (<span><i className="fa fa-money"></i> {label}</span>)
    })[url];
};


@withRouter
export default class ProfileMenu extends Component {
    static propTypes = {
        router: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        profileMenu: PropTypes.array.isRequired,
    };

    static defaultProps = {
        profileMenu: [
            { url: '/profile/', label: 'Profile' },
            {
                url: '/papers/',
                createUrl: '/papers/create',
                label: 'Papers',
                children: [{ url: '/papers/1', label: 'Papers' }]
            },
        ],
    };

    onSelect = (path) => {
        const { router } = this.props;
        router.push(path);
    };

    makeSubEntityMenu = ({ url, createUrl, label, children }) => {
        const { location: { pathname } } = this.props;
        const menuItems = children.map((item) => {
            return (
                <MenuItem key={item.url} className="truncate" eventKey={item.url}>
                    {item.label}
                </MenuItem>
            );
        });

        return (
            <NavDropdown
                key={name}
                className={classnames({ active: pathname.search(url) > -1 })}
                eventKey={createUrl}
                title={<span><i className={getIconClass(url)} /> {label}</span>}
                id={`id_${name}`}
            >
                {menuItems}
                {
                    createUrl && [
                        <MenuItem key="1" divider />,
                        <MenuItem key="1=2" eventKey={createUrl}> { 'Add' }</MenuItem>,
                    ]
                }
            </NavDropdown>
        );
    };

    render() {
        const { location: { pathname }, profileMenu } = this.props;

        let activeURL = '#';

        const menu = profileMenu.map(({ url, createUrl, label, children }) => {
            if (pathname.search(url) > -1) {
                activeURL = url;
            }

            if (children) {
                return this.makeSubEntityMenu({ url, createUrl, label, children });
            }

            return (
                <NavItem key={url} eventKey={url} href={url}>
                    <i className={getIconClass(url)} /> {label}
                </NavItem>
            );
        });

        return (
            <Nav className="ProfileMenu" bsStyle="pills" stacked activeKey={activeURL} onSelect={this.onSelect}>
                {menu}
            </Nav>
        );
    }
}
