export default {
    CONFIG_NAME: 'CI Staging',
    BASE_URL: 'http://odyoplug.demo.incode-systems.com',
    API_BASE_URL: 'http://odyoplug-dev.artyom.one',
    conference: process.env.CONFERENCE_ID || -1,
};
