import * as actionTypes from '../actions';
const initialState = {};


export default function patch(state = initialState, action = {}) {
    switch (action.type) {
        case actionTypes.SET_SCREEN:
            return { ...state, width: action.width, height: action.height };
        default:
            return state;
    }
}
