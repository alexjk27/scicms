import Cookies from 'js-cookie';
import * as actionTypes from '../actions';


const initialState = {
    profile: {},
    token: Cookies.get('jwt'),
};


export default function auth(state = initialState, action = {}) {
    switch (action.type) {
        case actionTypes.succeeded(actionTypes.SET_PROFILE_DATA):
            return { ...state, ...{ profile: { ...state.profile, ...action.result, isFetching: false } } };
        case actionTypes.failed(actionTypes.SET_PROFILE_DATA):
            return { ...state, ...{ profile: { ...state.profile, error: action.error, isFetching: false } } };
        case actionTypes.triggered(actionTypes.SET_PROFILE_DATA):
            return { ...state, ...{ profile: { ...state.profile, isFetching: true } } };

        case actionTypes.SET_TOKEN:
            return { ...state, token: action.token };

        // case actionTypes.LOGOUT:
        //     return { ...state, token: null, profile: {} };

        case actionTypes.AUTH_FAILED:
            return { ...state, authFailed: true, token: null, profile: {} };

        case actionTypes.failed(actionTypes.LOGIN):
            return { ...state, error: action.error, isFetching: false };
        case actionTypes.triggered(actionTypes.LOGIN):
            return { ...state, isFetching: true };
        default:
            return state;
    }
}
