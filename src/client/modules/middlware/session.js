import Cookies from 'js-cookie';
import * as actionTypes from '../actions';


export default function sessionMiddleware() {
    return next => (action) => {
        switch (action.type) {
            case actionTypes.SET_TOKEN:
                Cookies.set('jwt', action.token);
                break;

            case actionTypes.LOGOUT:
                Cookies.remove('jwt');
                //window.location.href = '/login';
                window.location.reload();
                break;

            case actionTypes.AUTH_FAILED:
                Cookies.remove('jwt');
                window.location.href = '/login';
                break;

            default:
        }

        return next(action);
    };
}
