import React, { Component, PropTypes } from 'react';
import { Panel } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../modules/actions';
import PaperForm from '../common/components/PaperForm';
import ConferenceHeader from '../common/components/ConferenceHeader';


class EditPaperPage extends Component {

    static propTypes = {
        registration: PropTypes.func.isRequired,
    };

    onSubmit = (values) => {
        this.props.registration(values);
    };

    render() {
        return (
            <div className="EditPaperPage">
                <ConferenceHeader />
                <div className="container">
                    <Panel>
                        <PaperForm onSubmit={this.onSubmit} {...this.props} />
                    </Panel>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        sections: state.conference.sections,
        presentationTypes: state.conference.presentationTypes,
        paperFields: state.conference.paperFields,
        initialValues: state.entities.paper,
    }),
    dispatch => ({
        registration: values => dispatch(actions.registration('paper', values)),
    }),
)(
    reduxForm({
        form: 'PaperForm',
        fields: ['paper'],
    })(EditPaperPage)
);
