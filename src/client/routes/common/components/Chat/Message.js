import React, { PureComponent, PropTypes } from 'react';
import { Media } from 'react-bootstrap';
import defaultAva from '../../../../public/images/user.png';


class Message extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        avatar: PropTypes.string,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
        onDelete: PropTypes.func.isRequired,
    };

    render() {
        const { id, avatar, first_name: firstName, last_name: lastName, message } = this.props;

        return (
            <Media>
                <Media.Left>
                    <img width={64} height={64} className="img-circle" src={avatar || defaultAva} alt="" />
                </Media.Left>
                <Media.Body>
                    <Media.Heading>
                        {lastName} {firstName}
                        <button type="button" onClick={() => this.props.onDelete(id)} className="close">
                            <span>&times;</span>
                        </button>
                    </Media.Heading>
                    <p>{message}</p>
                </Media.Body>
            </Media>
        );
    }
}


export default Message;
