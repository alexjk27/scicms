import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import { DragSource, DropTarget } from 'react-dnd';
import Card from '../components/Card';


let lastPosition = {};


function getStyles(isDragging) {
    return {
        opacity: isDragging ? 0.7 : 1
    };
}


const cardSource = {
    beginDrag(props, monitor, component) {
        const { item } = props;
        const { clientWidth, clientHeight } = findDOMNode(component);

        return { ...item, clientWidth, clientHeight };
    },
    endDrag(props) {
        props.stopScrolling();
    },
    isDragging(props, monitor) {
        const isDragging = props.item && props.item.id === monitor.getItem().id;
        return isDragging;
    }
};

// options: 4rd param to DragSource https://gaearon.github.io/react-dnd/docs-drag-source.html
const OPTIONS = {
    arePropsEqual: function arePropsEqual(props, otherProps) {
        let isEqual = true;
        if (props.item.id === otherProps.item.id &&
            props.x === otherProps.x &&
            props.y === otherProps.y
        ) {
            isEqual = true;
        } else {
            isEqual = false;
        }
        return isEqual;
    }
};


function collectDragSource(connectDragSource, monitor) {
    return {
        connectDragSource: connectDragSource.dragSource(),
        isDragging: monitor.isDragging()
    };
}

const cardTarget = {
    drop(props, monitor) {
        const { userId, id: cardId } = monitor.getItem();

        props.moveCard(userId, cardId, lastPosition);
    },
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().id;
        const hoverIndex = props.item.id;
        const { item: { beforePosition, position, afterPosition, desk } } = props;

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return;
        }

        // Get vertical middle
        const hoverMiddleY = (component.boundingRect.bottom + component.boundingRect.top) / 2;

        // Get pixels to the top
        const hoverClientY = monitor.getClientOffset().y - hoverMiddleY;

        component.setState({ placeholder: hoverClientY < 0 ? 'top' : 'bottom' });

        // Get pixels to the top
        if (component.state.placeholder === 'bottom') {
            // bottom
            lastPosition = {
                desk,
                beforePosition: position,
                position: (afterPosition + position) / 2,
                afterPosition,
            };
        } else {
            // top
            lastPosition = {
                desk,
                beforePosition,
                position: (beforePosition + position) / 2,
                afterPosition: position,
            };
        }
    }
};


@DragSource('card', cardSource, collectDragSource, OPTIONS)
@DropTarget('card', cardTarget, (connectDragSource, monitor) => ({
    connectDropTarget: connectDragSource.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    source: monitor.getItem() || { clientWidth: 0, clientHeight: 0 },
    clientOffset: monitor.getClientOffset(),
}))
export default class DraggableCard extends Component {
    static propTypes = {
        item: PropTypes.object,
        connectDragSource: PropTypes.func.isRequired,
        connectDropTarget: PropTypes.func.isRequired,
        isDragging: PropTypes.bool.isRequired,
        isOver: PropTypes.bool.isRequired,
        isOverCurrent: PropTypes.bool.isRequired,
        source: PropTypes.object.isRequired,
    };

    state = {
        placeholder: null,
    };

    componentWillReceiveProps(nextProps) {
        if (!this.props.isOver && nextProps.isOver) {
            // You can use this as enter handler
        }

        if (this.props.isOver && !nextProps.isOver) {
            this.setState({ placeholder: null });
        }

        if (this.props.isOverCurrent && !nextProps.isOverCurrent) {
            // You can be more specific and track enter/leave
            // shallowly, not including nested targets
        }
    }

    render() {
        const {
            isDragging,
            connectDragSource,
            item,
            connectDropTarget,
            source: { clientWidth: width, clientHeight: height }
        } = this.props;
        const style = { width, height };

        return connectDragSource(connectDropTarget(
            <div ref={node => (this.boundingRect = node && node.getBoundingClientRect())}>
                {
                    this.state.placeholder === 'top' && <div style={style} className="Card placeholder" />
                }
                <Card style={getStyles(isDragging)} item={item} />
                {
                    this.state.placeholder === 'bottom' && <div style={style} className="Card placeholder" />
                }
            </div>
        ));
    }
}
