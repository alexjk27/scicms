import * as actionTypes from '../actions/desk';


const initialState = {
    isFetching: false,
    lists: [],
    isDragging: false
};


export default function lists(state = initialState, action) {
    switch (action.type) {
        case actionTypes.GET_LISTS_START:
            return { ...state, isFetching: true };
        case actionTypes.GET_LISTS:
            return { ...state, isFetching: false, lists: action.lists };
        case actionTypes.MOVE_LIST: {
            const newLists = [...state.lists];
            const { lastX, nextX } = action;
            const t = newLists.splice(lastX, 1)[0];

            newLists.splice(nextX, 0, t);

            return { ...state, lists: newLists };
        }
        case actionTypes.TOGGLE_DRAGGING: {
            return { ...state, isDragging: action.isDragging };
        }
        default:
            return state;
    }
}
