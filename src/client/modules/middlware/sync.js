import jsonpatch from 'fast-json-patch';
import { submit } from '../services/http';
import { triggered, succeeded, failed } from '../actions';
import config from '../../config';


const sync = ({ dispatch, getState }) => next => (action) => {
    const { serverSync, ...rest } = action;

    if (!serverSync) {
        return next(action);
    }

    const prevState = getState();
    const { revision, auth: { token } } = prevState;
    const url = `${config.PATCH_ENDPOINT}${prevState.conference.id}/${revision}/`;

    dispatch({ ...rest, revision });

    const patch = jsonpatch.compare(prevState, getState());
    const { type } = rest;

    dispatch({
        ...rest,
        revision,
        type: triggered(type),
        baseType: type,
    });

    return submit(url, { toUser: action.toUser, patch })(token)
        .then(({ rev }) => {
            dispatch({
                ...rest,
                type: 'SET_REVISION',
                rev,
            });
            const result = next({
                ...rest,
                type: succeeded(type),
                baseType: type,
            });
            return result;
        }).catch((error) => {
            dispatch({
                ...rest,
                prevState,
                type: failed(type),
                baseType: type,
                error: {
                    ...error.response,
                    statusCode: error.response && error.response.status,
                },
            });
            throw error;
        });
};

export default sync;
