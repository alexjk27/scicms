import React, { PropTypes, Component } from 'react';
import Autosuggest from 'react-autosuggest';
import './AutosuggestInput.less';


// Teach Autosuggest how to calculate suggestions for any given input value.
const getSuggestions = (items, value) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : items.filter((item) => {
        let result = item.label.toLowerCase().indexOf(inputValue) > -1;

        if (!result) {
            Object.keys(item.translations).forEach((key) => {
                result = item.translations[key].toLowerCase().indexOf(inputValue) > -1;
                return !result;
            });
        }
        return result;
    });
};

// When suggestion is clicked, Autosuggest needs to populate the input element
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.label;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
    <div>
        {suggestion.label}
        <div>
            <small>{suggestion.translations.en}</small>
        </div>
    </div>
);


export default class AutosuggestField extends Component {
    static propTypes = {
        options: PropTypes.array,
        onChange: PropTypes.func,
        value: PropTypes.string,
    };

    constructor() {
        super();

        // Autosuggest is a controlled component.
        // This means that you need to provide an input value
        // and an onChange handler that updates this value (see below).
        // Suggestions also need to be provided to the Autosuggest,
        // and they are initially empty because the Autosuggest is closed.
        this.state = {
            value: '',
            suggestions: []
        };
    }

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(this.props.options, value)
        });
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const { suggestions } = this.state;

        // Autosuggest will pass through all these props to the input element.
        const inputProps = {
            className: 'form-control',
            type: 'search',
            //...this.props,
            value: this.props.value,
            onChange: (e, { newValue, method }) => {
                if (['click', 'enter'].indexOf(method) > -1) {
                    this.props.onChange(newValue.trim());
                } else {
                    this.props.onChange(e);
                }
            }
        };

        // Finally, render it!
        return (
            <Autosuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
            />
        );
    }
}
