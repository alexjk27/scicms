import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../modules/actions';
import UserForm from '../common/components/UserForm';
import MessageForm from '../common/components/MessageForm';


class Home extends Component {
    static propTypes = {
        count: PropTypes.number,
        dispatch: PropTypes.func,
        error: PropTypes.object,
        triggered: PropTypes.bool,
        user: PropTypes.object,
        messages: PropTypes.array,
    };

    render() {
        const { increment, decrement, setEntity, pusMsg } = bindActionCreators(actions, this.props.dispatch);
        const { user, messages } = this.props;

        return (
            <div className="container">
                Count {this.props.count} <i className="fa fa-check" />
                <p>
                    <button onClick={increment}>Increment</button>
                    <button onClick={decrement}>Decrement</button>
                </p>
                <p>Error: {this.props.error && this.props.error.statusText}</p>
                {
                    this.props.triggered &&
                    <i className="fa fa-spinner fa-spin fa-3x fa-fw" />
                }
                <div>
                    <UserForm initialValues={user} onSubmit={values => setEntity('user', values)} />
                </div>
                <div>
                    <div><b>First name</b>: {user.firstName}</div>
                    <div><b>Last name</b>: {user.lastName}</div>
                </div>
                <div>
                    <h3>Messages:</h3>
                    <ol>
                        {
                            _.map(messages, (message, i) => {
                                return <li key={i}>{message}</li>;
                            })
                        }
                    </ol>
                    <div>
                        <MessageForm onSubmit={pusMsg} />
                    </div>
                </div>
            </div>
        );
    }
}


export default connect(state => ({
    count: state.counter.counter,
    user: state.entities.user || {},
    messages: state.messages.items,
    triggered: state.counter.triggered,
    fail: state.counter.fail,
    error: state.counter.error,
}))(Home);
