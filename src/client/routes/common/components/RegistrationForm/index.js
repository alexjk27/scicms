import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import { Form, Alert } from 'react-bootstrap';
import { renderField, SubmitButton } from '../../../../helpers/ui';
import { testRequired, email, confirmPassword } from '../.././../../helpers/validate';


export default class RegistrationForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        error: PropTypes.string.isRequired,
    };

    render() {
        const {
            handleSubmit,
            error,
        } = this.props;

        return (
            <div className="ProfileForm">
                <Form
                    horizontal
                    className="form"
                    role="form"
                    method="post"
                    acceptCharset="UTF-8"
                    onSubmit={handleSubmit(this.props.onSubmit)}
                >
                    {
                        error && (
                            <Alert bsStyle="danger"><strong>{error}</strong></Alert>
                        )
                    }
                    <Field
                        name="first_name"
                        type="text"
                        component={renderField}
                        placeholder={'First name'}
                        label={'First name'}
                    />
                    <Field
                        name="last_name"
                        type="text"
                        component={renderField}
                        placeholder={'Last name'}
                        label={'Last name'}
                    />
                    <Field
                        name="email"
                        type="text"
                        component={renderField}
                        placeholder={'Email'}
                        label={'Email'}
                        required
                        validate={[testRequired, email]}
                    />
                    <Field
                        name="password"
                        type="password"
                        component={renderField}
                        placeholder={'Password'}
                        label={'Password'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="confirmPassword"
                        type="password"
                        component={renderField}
                        placeholder={'Confirm Password'}
                        label={'Confirm Password'}
                        required
                        validate={[testRequired, confirmPassword]}
                    />
                    <SubmitButton {...this.props} />
                </Form>
            </div>
        );
    }
}
