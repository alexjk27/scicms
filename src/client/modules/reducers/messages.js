import _ from 'lodash';
import { reduceTriggeredAction, SET_MESSAGE, DELETE_MESSAGE } from '../actions';


const initialState = {
    fail: null,
    lastUpdate: (new Date()).getTime(),
    triggered: false,
    error: {},
    items: {},
};


export default function messages(state = initialState, action = {}) {
    switch (action.type) {
        case SET_MESSAGE:
            return {
                ...state,
                items: { ...state.items, [action.id]: { ...action.message, id: action.id, order: action.revision } }
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                items: { ..._.pickBy(state.items, (item, id) => id !== action.id) },
            };
        default:
            return reduceTriggeredAction(state, action, [SET_MESSAGE]);
    }
}
