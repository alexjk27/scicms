import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Alert, FormGroup, FormControl, HelpBlock, Checkbox, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../../../../modules/actions';


function renderField({ input, placeholder, type, meta: { touched, error } }) {
    let validationState;
    if (touched && error) {
        validationState = 'error';
    } else if (touched && !error) {
        validationState = 'success';
    } else {
        validationState = null;
    }

    if (type === 'checkbox') {
        return (
            <Checkbox
                {...input}
                validationState={validationState}
            >
                Remember me
            </Checkbox>
        );
    }
    return (
        <FormGroup validationState={validationState}>
            <FormControl
                placeholder={placeholder}
                type={type} {...input}
            />
            {error && <HelpBlock>{error}</HelpBlock>}
        </FormGroup>
    );
}


renderField.propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
    placeholder: PropTypes.string,
    type: PropTypes.string.isRequired,
};


@connect()
class EmailSignInForm extends Component {

    static propTypes = {
        submitting: PropTypes.bool.isRequired,
        pristine: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        error: PropTypes.string,
        onSuccess: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired,
    };

    onSubmit = (values) => {
        return this.props.dispatch(actions.login(values)).then((data) => {
            this.props.onSuccess && this.props.onSuccess(data);
        });
    };

    render() {
        const {
            error,
            submitting,
            pristine,
            handleSubmit
        } = this.props;

        return (
            <div className="EmailSignInForm row">
                <div className="col-md-12">
                    {
                        error && (
                            <Alert bsStyle="danger"><strong>{error}</strong></Alert>
                        )
                    }
                    <form
                        className="form"
                        role="form"
                        method="post"
                        action="login"
                        acceptCharset="UTF-8"
                        id="login-nav"
                        onSubmit={handleSubmit(this.onSubmit)}
                    >
                        <Field
                            name="email"
                            type="text"
                            component={renderField}
                            placeholder={'Username'}
                        />
                        <Field
                            name="password"
                            type="password"
                            component={renderField}
                            placeholder={'Password'}
                        />
                        <Field
                            name="rememberMe"
                            type="checkbox"
                            component={renderField}
                        />
                        <div className="form-group">
                            <Button
                                type="submit"
                                disabled={pristine || submitting}
                                bsStyle="primary"
                                block
                            >{ submitting && <i className="fa fa-spinner fa-spin" /> } Sign in</Button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default reduxForm({
    form: 'LoginForm',
})(EmailSignInForm);
