import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { splitEntities } from '../../helpers/utils';
import { moveCard, moveList, getLists } from '../../modules/actions';
import CardsContainer from './containers/CardsContainer';
import './DeskPage.less';


function mapStateToProps(state) {
    return {
        cards: splitEntities(state.users), //splitEntities(state.users, 'paper'),
        desks: state.desk.lists
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ moveCard, moveList, getLists }, dispatch);
}


@connect(mapStateToProps, mapDispatchToProps)
@DragDropContext(HTML5Backend)
export default class DeskPage extends Component {
    static propTypes = {
        getLists: PropTypes.func.isRequired,
        moveCard: PropTypes.func.isRequired,
        moveList: PropTypes.func.isRequired,
        desks: PropTypes.array.isRequired,
        cards: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        this.moveList = this.moveList.bind(this);
        this.findList = this.findList.bind(this);
        this.scrollRight = this.scrollRight.bind(this);
        this.scrollLeft = this.scrollLeft.bind(this);
        this.stopScrolling = this.stopScrolling.bind(this);
        this.startScrolling = this.startScrolling.bind(this);
        this.state = { isScrolling: false };
    }

    componentWillMount() {
        this.props.getLists(1);
    }

    startScrolling(direction) {
        // if (!this.state.isScrolling) {
        switch (direction) {
            case 'toLeft':
                //this.setState({ isScrolling: true }, this.scrollLeft());
                break;
            case 'toRight':
                //this.setState({ isScrolling: true }, this.scrollRight());
                break;
            default:
                break;
        }
        // }
    }

    scrollRight() {
        function scroll() {
            document.getElementsByTagName('main')[0].scrollLeft += 10;
        }

        this.scrollInterval = setInterval(scroll, 10);
    }

    scrollLeft() {
        function scroll() {
            document.getElementsByTagName('main')[0].scrollLeft -= 10;
        }

        this.scrollInterval = setInterval(scroll, 10);
    }

    stopScrolling() {
        //this.setState({ isScrolling: false }, clearInterval(this.scrollInterval));
    }

    moveList(listId, nextX) {
        const { lastX } = this.findList(listId);
        this.props.moveList(lastX, nextX);
    }

    findList(id) {
        const { desks } = this.props;
        const list = desks.filter(l => l.id === id)[0];

        return {
            list,
            lastX: desks.indexOf(list)
        };
    }

    render() {
        const { desks, cards } = this.props;

        return (
            <div style={{ height: '100%' }}>
                {desks.map((desk, i) =>
                    <CardsContainer
                        key={desk.id}
                        id={desk.id}
                        x={i}
                        desk={desk}
                        cards={cards[desk.id] || []}
                        moveCard={this.props.moveCard}
                        moveList={this.moveList}
                        startScrolling={this.startScrolling}
                        stopScrolling={this.stopScrolling}
                        isScrolling={this.state.isScrolling}
                    />
                )}
            </div>
        );
    }
}
