import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from '../../../../modules/actions';
import ProfileForm from '../../components/ProfileForm';
import { toCamelCaseObject } from '../../../../helpers/parsers';
import './EditProfileModal.less';


class EditProfileModal extends Component {
    static propTypes = {
        setEntity: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className="EditProfileModal clearfix">
                <ProfileForm onSubmit={this.props.setEntity} {...this.props} />
            </div>
        );
    }
}


export default connect(
    state => ({
        scientificDegree: state.conference.scientificDegree,
        academicRang: state.conference.academicRang,
        initialValues: (
            toCamelCaseObject((state.users[state.auth.profile.email] || {}).profile || state.auth.profile)
        ),
    }),
    dispatch => ({
        setEntity: values => dispatch(actions.setEntity('profile', values)),
    }),
)(
    reduxForm({
        form: 'EditProfileModal',
    })(EditProfileModal)
);
