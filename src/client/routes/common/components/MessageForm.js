import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';


const UserForm = (props) => {
    const { handleSubmit, pristine, submitting } = props;

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="idMessage">Message</label>
                <div>
                    <Field id="idMessage" name="message" component="textarea" placeholder="Message" />
                </div>
            </div>
            <div>
                <button type="submit" disabled={pristine || submitting}>Submit</button>
            </div>
        </form>
    );
};

UserForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
};


export default reduxForm({
    form: 'messageForm'
})(UserForm);
