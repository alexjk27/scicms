import Express from 'express';
import path from 'path';
import http from 'http';
import cookieParser from 'cookie-parser';
import isomorphic from './client/isomorphic';

const PORT = 8090;
const app = Express();
const server = new http.Server(app);


app.disable('etag');

//app.enable('view cache');
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'pug');

app.use(cookieParser());
app.use('/static', Express.static(path.join(__dirname, '../static')));
app.use('/', isomorphic);


// Start the HTTP server
server.listen(PORT, '0.0.0.0', (err) => {
    if (err) {
        console.log(err);
    }
    console.log(`Listening at 0.0.0.0:${PORT}`);
});
