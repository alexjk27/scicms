import React, { PureComponent, PropTypes } from 'react';
import { reduxForm, Field, reset } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../../../modules/actions';
import { SubmitButton, renderField } from '../../../../helpers/ui';


@connect()
class MessageInput extends PureComponent {
    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired,
        userId: PropTypes.number,
    };

    onSubmit = (values) => {
        this.props.dispatch(actions.setMsg(values, this.props.userId));
        this.props.dispatch(reset('messages'));
    };

    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit)} className="MessageInput clearfix">
                <Field
                    name="message"
                    type="textarea"
                    component={renderField}
                    placeholder={'Send message...'}
                />
                <SubmitButton {...this.props} onSubmit={this.onSubmit} />
            </form>
        );
    }
}


export default reduxForm({
    form: 'messages'
})(MessageInput);
