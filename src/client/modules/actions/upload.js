import { upload } from '../services/http';
import appConfig from '../../config';


export const UPLOADED_FILES = 'UPLOADED_FILES';
export const UPLOADED_FILE_PROGRESS = 'UPLOADED_FILE_PROGRESS';


export function uploadProgress(field, progress) {
    return { type: UPLOADED_FILE_PROGRESS, field, progress };
}


export function uploadFile(field, files) {

    return async function (dispatch, getState) {
        const progress = (evt) => {
            const pr = (evt.loaded / evt.total) * 100;
            dispatch(uploadProgress(field, pr));
        };

        const params = {
            progress,
            url: appConfig.UPLOADS_ENDPOINT,
            files,
        };
        const { auth: { token } } = getState();
        const file = await upload(params)(token);

        return dispatch({ type: UPLOADED_FILES, field, file });
    };
}
