import appConf from '../../config';
import { submit, get } from '../services/http';
import { loadPatch } from './patches';


export const SET_TOKEN = 'SET_TOKEN';
export function setToken(token) {
    return {
        type: SET_TOKEN,
        token
    };
}

export const AUTH_FAILED = 'AUTH_FAILED';
export function authFailed() {
    return {
        type: AUTH_FAILED,
    };
}

export const SET_PROFILE_DATA = 'SET_PROFILE_DATA';
export function setProfileData() {
    return {
        type: SET_PROFILE_DATA,
        api: get(appConf.PROFILE_ENDPOINT),
    };
}


export const LOGIN = 'LOGIN';
export function login(values) {
    return {
        type: LOGIN,
        api: submit(appConf.LOGIN_ENDPOINT, values),
        onSuccess: ({ result, dispatch }) => {
            dispatch(setToken(result.token));
            return dispatch(setProfileData()).then(() => {
                return dispatch(loadPatch(['/users']));
            });
        }
    };
}


export const REGISTRATION = 'REGISTRATION';
export function registration(values) {
    return {
        type: REGISTRATION,
        api: submit(appConf.REGISTRATION_ENDPOINT, values),
        onSuccess: ({ dispatch }) => {
            return dispatch(login(values));
        }
    };
}

export const LOGOUT = 'LOGOUT';
export function logout() {
    return { type: LOGOUT };
}
