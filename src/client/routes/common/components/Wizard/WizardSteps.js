import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import './WizardSteps.less';


export default class WizardSteps extends Component {
    static propTypes = {
        steps: PropTypes.array.isRequired,
        currentStep: PropTypes.number.isRequired,
        onChoice: PropTypes.func,
    };

    render() {
        const { currentStep, onChoice } = this.props;
        const steps = this.props.steps.map((item, index) => {
            return (
                <button
                    key={index}
                    onClick={() => onChoice && onChoice(index, item)}
                    className={classnames({ 'WizardSteps-current': index === currentStep })}
                >
                    <span className="badge badge-inverse">{index + 1}</span>
                    {item.title}
                    { item.isValid && <i className="fa fa-check" /> }
                </button>
            );
        });

        return (
            <div className="WizardSteps">
                {steps}
            </div>
        );
    }
}
