import faker from 'faker';

export const GET_LISTS = 'GET_LISTS';
export const GET_LISTS_START = 'GET_LISTS_START';
export const MOVE_CARD = 'MOVE_CARD';
export const MOVE_LIST = 'MOVE_LIST';
export const TOGGLE_DRAGGING = 'TOGGLE_DRAGGING';


export function getLists(quantity) {
    return (dispatch) => {
        dispatch({ type: GET_LISTS_START, quantity });
        setTimeout(() => {
            const lists = [
                {
                    id: 'default',
                    name: 'Default',
                }
            ];
            for (let i = 0; i < quantity; i++) {
                lists.push({
                    id: i,
                    name: faker.commerce.productName(),
                });
            }
            dispatch({ type: GET_LISTS, lists, isFetching: true });
        }, 100); // fake delay
        dispatch({ type: GET_LISTS_START, isFetching: false });
    };
}

export function moveList(lastX, nextX) {
    return (dispatch) => {
        dispatch({ type: MOVE_LIST, lastX, nextX });
    };
}

export function moveCard(userId, cardId, position) {
    return (dispatch) => {
        return dispatch({ type: MOVE_CARD, userId, cardId, position });
    };
}

export function toggleDragging(isDragging) {
    return (dispatch) => {
        dispatch({ type: TOGGLE_DRAGGING, isDragging });
    };
}
