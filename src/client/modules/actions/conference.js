export const SET_ACADEMIC_RANG_LIST = 'SET_ACADEMIC_RANG_LIST';


export function setAcademicRangs(items) {
    return {
        type: SET_ACADEMIC_RANG_LIST,
        items
    };
}
