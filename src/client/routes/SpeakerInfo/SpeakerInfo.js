import React, { PropTypes, Component } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import ModalLink from '../common/components/ModalLink';
import SpeakerModal from '../common/components/SpeakerModal';
import { toCamelCaseObject } from '../../helpers/parsers';
import userPic from '../../public/images/user.png';
import './SpeakerInfo.less';


class SpeakerInfo extends Component {
    static propTypes = {
        profile: PropTypes.object.isRequired,
        sCard: PropTypes.object.isRequired,
    };

    render() {
        const {
            profile: { firstName, lastName, patronymic, birthday, avatar, email },
            sCard,
        } = this.props;
        const fullName = (`${firstName} ${lastName} ${patronymic || ''}`).trim();

        return (
            <div className="SpeakerInfo">
                <SpeakerModal modalName="speakerModal" title={fullName} />

                <h3>
                    {fullName}
                    <small>({moment().diff(birthday, 'years')} {gettext('years')})</small>
                    <ModalLink className="pull-right btn btn-info SpeakerInfo-edit" modal="speakerModal" >
                        <i className="fa fa-edit" /> Edit
                    </ModalLink>
                </h3>
                <table className="">
                    <tbody>
                        <tr>
                            <td rowSpan="7" className="SpeakerInfo-avatar">
                                <img
                                    className="img-thumbnail"
                                    width="100"
                                    height="100"
                                    src={avatar || userPic}
                                    alt={fullName}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td><em>
                                {email}
                            </em></td>
                        </tr>

                        <tr className="subhead">
                            <th className="text-center">{gettext('Email')}</th>
                        </tr>

                        <tr>
                            <td><em>
                                {sCard.phone || '*(***) ***-**-**'}
                            </em></td>
                        </tr>

                        <tr className="subhead">
                            <th className="text-center">{gettext('Phone')}</th>
                        </tr>

                        <tr>
                            <td><em>
                                {sCard.mail}
                            </em></td>
                        </tr>

                        <tr className="subhead">
                            <th colSpan="3" className="text-center">{gettext('Postal address')}</th>
                        </tr>

                        <tr>
                            <td colSpan="2"><em>
                                {sCard.scientificDegree},
                                {sCard.academicRang},
                                {sCard.officialCapacity}
                            </em></td>
                        </tr>

                        <tr className="subhead">
                            <th colSpan="2" className="text-center">
                                {gettext('Scientific degree, academic rank, official position')}
                            </th>
                        </tr>

                        <tr>
                            <td colSpan="2" className=""><em>
                                {sCard.workPlace},
                            </em></td>
                        </tr>

                        <tr className="subhead">
                            <th colSpan="2" className="text-center">{gettext('job')}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}


export default connect(
    state => ({
        profile: (
            toCamelCaseObject((state.users[state.auth.profile.email] || {}).profile || state.auth.profile)
        ),
        sCard: (state.users[state.auth.profile.email] || {}).sCard || {}
    }),
)(SpeakerInfo);

