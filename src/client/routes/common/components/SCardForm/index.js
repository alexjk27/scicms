import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import { Form } from 'react-bootstrap';

import { renderField, SubmitButton } from '../../../../helpers/ui';
import { testRequired } from '../../../../helpers/validate';
import './SCardForm.less';


export default class SCardForm extends Component {

    static propTypes = {
        scientificDegree: PropTypes.array.isRequired,
        academicRang: PropTypes.array.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
    };

    render() {
        const {
            handleSubmit,
            scientificDegree,
            academicRang,
            onSubmit,
        } = this.props;

        return (
            <div className="SCardForm">
                <Form
                    horizontal
                    className="form"
                    role="form"
                    method="post"
                    acceptCharset="UTF-8"
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Field
                        name="scientificDegree"
                        type="autosuggest"
                        options={scientificDegree}
                        component={renderField}
                        placeholder={'Scientific Degree'}
                        label={'Scientific Degree'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="academicRang"
                        type="autosuggest"
                        options={academicRang}
                        component={renderField}
                        placeholder={'Academic Rang'}
                        label={'Academic Rang'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="officialCapacity"
                        type="input"
                        component={renderField}
                        placeholder={'Official capacity'}
                        label={'Official capacity'}
                        required
                        validate={testRequired}
                    />
                    <fieldset>
                        <legend>Contact info</legend>
                        <Field
                            name="phone"
                            type="phone"
                            component={renderField}
                            placeholder={'Phone'}
                            label={'Phone'}
                        />
                        <Field
                            name="workPlace"
                            type="textarea"
                            component={renderField}
                            placeholder={'Work place'}
                            label={'Work place'}
                            required
                            validate={testRequired}
                        />
                        <Field
                            name="city"
                            type="input"
                            component={renderField}
                            placeholder={'City'}
                            label={'City'}
                            required
                            validate={testRequired}
                        />
                        <Field
                            name="mail"
                            type="textarea"
                            component={renderField}
                            placeholder={'Mail'}
                            label={'Mail'}
                            required
                            validate={testRequired}
                        />
                    </fieldset>
                    <SubmitButton {...this.props} />
                </Form>
            </div>
        );
    }
}
