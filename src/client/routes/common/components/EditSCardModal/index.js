import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from '../../../../modules/actions';
import SCardForm from '../../components/SCardForm';
import { toCamelCaseObject } from '../../../../helpers/parsers';
import './EditSCardModal.less';


class EditSCardModal extends Component {
    static propTypes = {
        setEntity: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className="EditSCardModal clearfix">
                <SCardForm onSubmit={this.props.setEntity} {...this.props} />
            </div>
        );
    }
}


export default connect(
    state => ({
        scientificDegree: state.conference.scientificDegree,
        academicRang: state.conference.academicRang,
        initialValues: (
            toCamelCaseObject((state.users[state.auth.profile.email] || {}).sCard || {})
        ),
    }),
    dispatch => ({
        setEntity: values => dispatch(actions.setEntity('sCard', values)),
    }),
)(
    reduxForm({
        form: 'EditSCardForm',
    })(EditSCardModal)
);
