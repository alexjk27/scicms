import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import WizardSteps from './WizardSteps';


export default class Wizard extends Component {
    static propTypes = {
        initialValues: PropTypes.object.isRequired,
        form: PropTypes.string.isRequired,
        children: React.PropTypes.array.isRequired,
        onSuccess: React.PropTypes.func.isRequired,
    };

    static contextTypes = {
        store: React.PropTypes.object.isRequired
    };

    state = {
        page: 0,
        values: {},
    };

    onSubmit = (name, values) => {
        this.state.values[name] = this.getValues(values);
        this.props.onSuccess(this.state.values);
    };

    onNextPage = (name, values) => {
        this.setState({
            page: this.state.page + 1,
            values: { ...this.state.values, [name]: this.getValues(values) }
        });
    };

    getValues = (values) => {
        const { form: { [this.props.form]: { registeredFields } } } = this.context.store.getState();
        return _.fromPairs(_.map(registeredFields, ({ name }) => [name, values[name]]));
    };

    previousPage = () => {
        this.setState({ page: this.state.page - 1 });
    };

    render() {
        const { page } = this.state;
        const { children, initialValues, form } = this.props;
        const currentPage = children[page];

        return (
            <div>
                <WizardSteps
                    currentStep={page}
                    steps={children.map(item => item.props)}
                />
                {
                    React.cloneElement(currentPage, {
                        initialValues,
                        form,
                        onSubmit: children.length - 1 === page ? this.onSubmit : this.onNextPage,
                        previousPage: page > 0 ? this.previousPage : null,
                    })
                }
            </div>
        );
    }
}
