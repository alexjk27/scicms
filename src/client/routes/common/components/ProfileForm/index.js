import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import { Form } from 'react-bootstrap';

import { renderField, SubmitButton } from '../../../../helpers/ui';
import { testRequired } from '../../../../helpers/validate';
import './ProfileForm.less';


export default class ProfileForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
    };

    render() {
        const {
            handleSubmit,
            onSubmit,
        } = this.props;

        return (
            <div className="ProfileForm">
                <Form
                    horizontal
                    className="form"
                    role="form"
                    method="post"
                    acceptCharset="UTF-8"
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Field
                        name="firstName"
                        type="text"
                        component={renderField}
                        placeholder={'First name'}
                        label={'First name'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="lastName"
                        type="text"
                        component={renderField}
                        placeholder={'Last name'}
                        label={'Last name'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="patronymic"
                        type="text"
                        component={renderField}
                        placeholder={'Patronymic'}
                        label={'Patronymic'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="birthday"
                        type="date"
                        component={renderField}
                        placeholder={'Birthday'}
                        label={'Birthday'}
                        required
                        validate={testRequired}
                    />
                    <SubmitButton {...this.props} />
                </Form>
            </div>
        );
    }
}
