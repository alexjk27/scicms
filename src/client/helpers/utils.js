import _ from 'lodash';


export function splitEntities(users, name) {
    let cards = [];
    _.forEach(users, user => (cards = [...cards, ...user.cards.filter(c => (name ? c.type === name : true))]));
    cards = _.sortBy(cards, 'position');
    return _.groupBy(cards, card => (card.desk !== null ? card.desk : 'default'));
}


// export function log(...args) {
//     console.log(args);
// }
