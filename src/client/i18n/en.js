export default {
    venue: 'Venue',
    features: 'Features',
    pricing: 'Pricing',
    contactUs: 'Contact us',
    tac: 'Terms and Conditions',
    pp: 'Privacy Policy',
    support: 'Support',
    signIn: 'Sign in',
    login: 'Login',
    register: 'Register',
    fu: 'Forgot username',
    fp: 'Forgot password',
    rememberMe: 'Remember Me'
};
