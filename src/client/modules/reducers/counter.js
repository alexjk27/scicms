import { reduceTriggeredAction, INCREMENT, DECREMENT } from '../actions';


const initialState = { fail: null, counter: 0, lastUpdate: (new Date()).getTime(), triggered: false, error: {} };


export default function counter(state = initialState, action = {}) {
    switch (action.type) {
        case INCREMENT:
            return { ...state, counter: state.counter + 1 };
        case DECREMENT:
            return { ...state, counter: state.counter - 1 };
        default:
            return reduceTriggeredAction(state, action, [INCREMENT, DECREMENT]);
    }
}
