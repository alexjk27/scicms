import React, { PropTypes, Component } from 'react';
import Dropzone from 'react-dropzone';
import { Button } from 'react-bootstrap';
import defaultBackground from './images/rainbow_bk.jpg';
import defaultLogo from './images/conference-256.png';
import './ConferenceHeader.less';


export default class ConferenceHeader extends Component {
    static propTypes = {
        conference: PropTypes.object.isRequired,
        user: PropTypes.object.isRequired,
        updateConference: PropTypes.func.isRequired,
    };

    static defaultProps = {
        conference: {
            logo: defaultLogo,
            name: 'Default name',
            description: 'Default description',
            background: defaultBackground,
        },
        user: {
            isSecretary: false,
        }
    };

    onDropLogo = (files) => {
        this.fileUpdate('logo', files[0]);
    };

    onDropBg = (files) => {
        this.fileUpdate('background', files[0]);
    };

    onChangeName = (event) => {
        this.textUpdate('name', event);
    };

    onChangeDescription = (event) => {
        this.textUpdate('description', event);
    };

    textUpdate = (name, event) => {
        const { textContent } = event.target;
        const text = this.props.conference.get(name);

        if (text !== textContent) {
            this.props.updateConference({
                id: this.props.conference.get('id'),
                slug: this.props.conference.get('slug'),
                [name]: textContent
            });
        }
    };

    fileUpdate = (name, file) => {
        this.props.updateConference({
            id: this.props.conference.get('id'),
            slug: this.props.conference.get('slug'),
            name: this.props.conference.get('name'),
            [name]: file
        });
    };

    render() {
        const { conference: { logo, name, description, background }, user: { isSecretary } } = this.props;

        return (
            <div className="ConferenceHeader" style={{ background: `url(${background})` }}>
                {
                    isSecretary &&
                    (
                        <Dropzone
                            style={{}} activeStyle={{}} multiple={false} onDrop={this.onDropBg}
                        >
                            <Button>{'Replace picture'}</Button>
                        </Dropzone>
                    )
                }
                <div className="ConferenceHeader-logo">
                    {
                        isSecretary ?
                            (
                                <Dropzone
                                    style={{}} activeStyle={{}} onDrop={this.onDropLogo}
                                    className="ConferenceHeader-logo thumbnail" multiple={false}
                                >
                                    <img src={logo} alt={description} />
                                </Dropzone>
                            ) :
                            (
                                <div className="ConferenceHeader-logo thumbnail">
                                    <img src={logo} alt={description} />
                                </div>
                            )
                    }
                </div>
                <div>
                    <h2 className="ConferenceHeader-head">
                        <span
                            ref={com => (this.name = com)}
                            contentEditable={isSecretary}
                            onBlur={this.onChangeName}
                        >{name}</span>
                        {' '}
                        {isSecretary && <i className="fa fa-pencil" />}
                    </h2>
                </div>
                <div>
                    <h3 className="ConferenceHeader-head">
                        <span
                            ref={com => (this.description = com)} contentEditable={isSecretary}
                            onBlur={this.onChangeDescription}
                        >{description}</span>
                        {' '}
                        {isSecretary && <i className="fa fa-pencil" />}
                    </h3>
                </div>
            </div>
        );
    }
}
