import uuid from 'uuid/v1';


export const SET_MESSAGE = 'SET_MESSAGE';
export function setMsg({ message }, toUser) {
    return (dispatch, getstate) => {
        const { auth: { avatar, username, first_name, last_name, patronymic } } = getstate();

        const data = {
            message,
            avatar,
            username,
            first_name,
            last_name,
            patronymic
        };

        return dispatch({
            type: SET_MESSAGE,
            isApi: true,
            id: uuid(),
            message: data,
            toUser,
        });
    };
}


export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export function deleteMsg(id) {
    return {
        type: DELETE_MESSAGE,
        isApi: true,
        id
    };
}
