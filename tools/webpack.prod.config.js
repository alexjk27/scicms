var _ = require('lodash');
var path = require('path');
var webpack = require('webpack');
var config = require('./webpack.config');
var packageJSON = require('../package.json');


module.exports = _.merge({}, config, {
    devtool: false,
    entry: {
        main: path.join(__dirname, '../src/client.js'),
        vendor: Object.keys(packageJSON.dependencies),
    },
    output: {
        path: path.join(__dirname, '../dist/static/'),
        filename: 'js/[name].js',
        publicPath: '/static/'
    },
    plugins: config.plugins.concat([
        new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            comments: false,
            sourceMap: false
        })
    ]),
});
