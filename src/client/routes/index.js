import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './common/App';
import HomePage from './HomePage';
import ProfilePage from './ProfilePage';
import TestPage from './TestPage';
import NotFoundPage from './NotFoundPage';
import SpeakerInfo from './SpeakerInfo';
import EditSCardPage from './EditSCardPage';
import LoginPage from './LoginPage';
import RegistrationPage from './RegistrationPage';
import EditPaperPage from './EditPaperPage';
import ParticipantWizard from './ParticipantWizard';
import EditParticipantProfilePage from './EditParticipantProfilePage';
import ParticipantChat from './ParticipantChat';
import DeskPage from './DeskPage';
import { authRequired, authNoRequired } from '../helpers/routes';


const routes = store => (
    <Route path="/" component={App}>
        <IndexRoute component={HomePage} />
        <Route path="registration" component={RegistrationPage} onEnter={authNoRequired(store)} />
        <Route path="login" component={LoginPage} onEnter={authNoRequired(store)} />
        <Route path="desk" component={DeskPage} />

        <Route path="profile/" component={ProfilePage} onEnter={authRequired(store)}>
            <IndexRoute component={SpeakerInfo} />
            <Route path="/edit/scard" component={EditSCardPage} />
            <Route path="/paper" component={EditPaperPage} />
            <Route path="/wizard" component={ParticipantWizard} />
            <Route path="/chat" component={ParticipantChat} />
        </Route>

        <Route path="test" component={TestPage} />
        <Route path="dashboard/">
            <Route path="participants/:id/edit/profile/" component={EditParticipantProfilePage} />
            <Route path="participants/:participantId/chat/" component={ParticipantChat} />
        </Route>
        <Route path="*" component={NotFoundPage} />
    </Route>
);


export default routes;

