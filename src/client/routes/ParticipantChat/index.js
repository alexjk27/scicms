import React, { PureComponent, PropTypes } from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as actions from '../../modules/actions';
import Messages from '../common/components/Chat/Messages';
import './ParticipantChat.less';


@connect()
class ParticipantChat extends PureComponent {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        params: PropTypes.object.isRequired,
    };

    componentWillMount() {
        const { params: { participantId } } = this.props;
        this.props.dispatch(actions.loadPaginatedPatch('/messages', 1, 4, participantId));
    }

    render() {
        const { params: { participantId } } = this.props;
        return (
            <div>
                <div className="ParticipantChat container">
                    <Row>
                        <Col sm={12} md={12} lg={12}>
                            <Messages userId={participantId && parseInt(participantId, 10)} />
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}


export default ParticipantChat;
