import appConf from '../../config';
import { get } from '../services/http';


export const APPLY_PATCH = 'APPLY_PATCH';
export function applyPatch(patch, prefix = '') {
    if (prefix) {
        patch.forEach((item) => {
            item.path = `${prefix}${item.path}`;
        });
    }

    return {
        type: APPLY_PATCH,
        patch,
    };
}


export const SET_REVISION = 'SET_REVISION';
export function setRevision(rev) {
    return {
        type: SET_REVISION,
        rev,
    };
}


export const LOAD_PATCH = 'LOAD_PATCH';
export function loadPatch(paths, paginated = false) {
    return {
        type: LOAD_PATCH,
        api: get(`${appConf.PATCH_ENDPOINT}${appConf.conference}`, { paths, paginated: paginated && 'on' }),
        onSuccess: ({ result, dispatch }) => {
            dispatch(setRevision(result.rev));
            dispatch(applyPatch(result.patch));
        }
    };
}
