import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { reduxForm } from 'redux-form';


export default class WizardPage extends Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        onSubmit: PropTypes.func,
        previousPage: PropTypes.func,
        component: PropTypes.func.isRequired,
    };

    render() {
        const { component, previousPage, onSubmit, name, ...rest } = this.props;
        const Form = reduxForm({
            destroyOnUnmount: false,
            forceUnregisterOnUnmount: true,
        })(component);

        return (
            <div className="WizardPage">
                <Form {...rest} onSubmit={values => onSubmit(name, values)} />
                {previousPage && <Button type="submit" onClick={previousPage}>Previous</Button>}
            </div>
        );
    }
}
