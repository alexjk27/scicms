var path = require('path');
var fs = require('fs');
var webpack = require('webpack');
var argv = require('minimist')(process.argv.slice(2));
var ExtractTextPlugin = require('extract-text-webpack-plugin');


var DEBUG = !argv.release;

//
// Common configuration chunk to be used for both
// client-side (app.js) and server-side (server.js) bundles
// -----------------------------------------------------------------------------


var config = {
    cache: DEBUG,
    devtool: DEBUG ? '#inline-source-map' : false,

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader', // 'babel-loader' is also a legal name to reference
                options: {
                    plugins: ['transform-decorators-legacy', 'transform-runtime'],
                    presets: [['es2015', { 'modules': false }], 'stage-0', 'react'],
                    cacheDirectory: '/tmp/'
                },
                exclude: /(node_modules|bower_components)/
            },
            {
                test: /\.json$/, loader: 'json-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    {
                        fallbackLoader: 'style-loader',
                        loader: 'css-loader'
                    }
                )
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader')
            },
            {
                test: /\.(png|gif|jpg|eot|ttf|svg)$/,
                loader: 'file-loader',
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
        ]
    }
};


module.exports = config;
