import React, { Component, PropTypes } from 'react';
import { Panel } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as actions from '../../modules/actions';
import RegistrationForm from '../common/components/RegistrationForm';
import ConferenceHeader from '../common/components/ConferenceHeader';


@connect()
@reduxForm({ form: 'RegistartionPage' })
@withRouter
export default class RegistrationPage extends Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        router: PropTypes.object.isRequired,
    };

    onSubmit = (values) => {
        return this.props.dispatch(actions.registration(values)).then(() => {
            this.props.router.push('/profile');
        });
    };

    render() {
        return (
            <div className="EditPaperPage">
                <ConferenceHeader />
                <div className="container">
                    <Panel>
                        <RegistrationForm onSubmit={this.onSubmit} {...this.props} />
                    </Panel>
                </div>
            </div>
        );
    }
}
