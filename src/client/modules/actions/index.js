/**
 * Action "triggered" type creator
 * @param  {string} type - action type
 * @return {string} new action type
 */
export function triggered(type) {
    return `${type}_REQUEST_TRIGGERED`;
}

/**
 * Action "succeeded" type creator
 * @param  {string} type - action type
 * @return {string} new action type
 */
export function succeeded(type) {
    return `${type}_REQUEST_SUCCESS`;
}

/**
 * Action "failed" type creator
 * @param  {string} type - action type
 * @return {string} new action type
 */
export function failed(type) {
    return `${type}_REQUEST_FAILURE`;
}

/**
 * Action applied wrapped type
 * @param  baseTypes - filtered action type
 * @param  {object} state - reducer state
 * @param  {object} action - reducer action
 * @return {object} new state
 */
export function reduceTriggeredAction(state, action, baseTypes) {
    let newState = state;

    baseTypes.forEach((baseType) => {
        switch (action.type) {
            case triggered(baseType):
                newState = { ...state, triggered: true };
                return false;
            case succeeded(baseType):
                newState = { ...state, ...action.data, lastUpdate: (new Date()).getTime(), triggered: false };
                return false;
            case failed(baseType):
                newState = { ...state, error: action.error, fail: true, triggered: false };
                return false;
            default:
                return true;
        }
    });

    return newState;
}


export * from './counter';
export * from './entities';
export * from './messages';
export * from './auth';
export * from './conference';
export * from './upload';
export * from './participants';
export * from './patches';
export * from './paginations';
export * from './desk';
export * from './device';
