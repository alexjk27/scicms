import * as actions from '../../actions';
import participant from './fixtures/participant.json';


const initialState = {
    ...participant,
    id: 1,
    sections: [
        {
            value: 1,
            label: 'Секция 1',
            translations: {
                en: 'Section 1'
            }
        },
        {
            value: 2,
            label: 'Секция 2',
            translations: {
                en: 'Section 2'
            }
        }
    ],
    presentationTypes: [
        {
            value: 1,
            label: 'Заочное участие',
            translations: {
                en: 'type 1'
            }
        },
        {
            value: 2,
            label: 'Стендовый доклад',
            translations: {
                en: 'type 2'
            }
        }
    ],
    paperFields: [
        {
            name: 'paper',
            label: 'Paper',
            help: 'Paper help',
            required: true,
            translations: {
                en: {
                    label: 'Paper en',
                    help: 'Paper help en',
                }
            }
        },
        {
            name: 'paperEn',
            label: 'Paper english',
            help: 'Paper english help',
            required: true,
            translations: {
                en: {
                    label: 'Paper english en',
                    help: 'Paper english help en',
                }
            }
        }
    ],
};


export default function conference(state = initialState, action = {}) {
    switch (action.type) {
        case actions.SET_ACADEMIC_RANG_LIST:
            return { ...state, participant: action.items };
        default:
            return state;
    }
}
