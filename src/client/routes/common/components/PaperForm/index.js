import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import { Form } from 'react-bootstrap';
import { renderField, SubmitButton } from '../../../../helpers/ui';
import { testRequired } from '../../../../helpers/validate';
import { tr } from '../../../../helpers/i18n';
import './PaperForm.less';


export default class PaperForm extends Component {

    static propTypes = {
        presentationTypes: PropTypes.array.isRequired,
        sections: PropTypes.array.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        paperFields: PropTypes.array.isRequired,
        form: PropTypes.string.isRequired,
    };

    renderFileInputs = () => {
        const { form, paperFields } = this.props;
        return (
            <fieldset>
                <legend>Attachments</legend>
                {
                    paperFields.map(fl =>
                        (
                            <Field
                                key={fl.name}
                                name={fl.name}
                                type="exFile"
                                fieldId={`${form}_${fl.name}`}
                                component={renderField}
                                help={tr(fl, 'help', 'en')}
                                label={tr(fl, 'label', 'ru')}
                                required={fl.required}
                                validate={fl.required ? [testRequired] : []}
                            />
                        )
                    )
                }
            </fieldset>
        );
    };

    render() {
        const {
            handleSubmit,
            presentationTypes,
            sections
        } = this.props;

        return (
            <div className="ProfileForm">
                <Form
                    horizontal
                    className="form"
                    role="form"
                    method="post"
                    acceptCharset="UTF-8"
                    onSubmit={handleSubmit(this.props.onSubmit)}
                >
                    <Field
                        name="name"
                        type="text"
                        component={renderField}
                        placeholder={'Name'}
                        label={'Name'}
                        required
                        validate={testRequired}
                    />
                    <Field
                        name="section"
                        type="select"
                        component={renderField}
                        options={sections}
                        required
                        validate={testRequired}
                        placeholder={'Section'}
                        label={'Section'}
                    />
                    <Field
                        name="presentationType"
                        type="select"
                        component={renderField}
                        options={presentationTypes}
                        required
                        validate={testRequired}
                        placeholder={'Presentation type'}
                        label={'Presentation type'}
                    />
                    {this.renderFileInputs()}
                    <SubmitButton {...this.props} />
                </Form>
            </div>
        );
    }
}
