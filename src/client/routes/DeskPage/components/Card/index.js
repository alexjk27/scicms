import React, { PropTypes } from 'react';
import cn from 'classnames';
import './Card.less';


const propTypes = {
    item: PropTypes.object.isRequired,
    style: PropTypes.object
};


const Card = (props) => {
    const { style, item } = props;
    const icon = cn('fa', {
        'fa-desktop': item.type === 'paper',
        'fa-book': item.type === 'article',
        'fa-check': item.type === 'equals',
        'fa-building': item.type === 'booking',
        'fa-money': item.type === 'billing',
    });

    return (
        <div style={style} className="Card" id={style ? item.id : null}>
            <div className="Card-name">
                <i className={icon} /> {item.name}
            </div>
            {
                item.options && (
                    <ul className="Card-options">
                        {item.options.map(o => <li key={o.id}>{o.label}</li>)}
                    </ul>
                )
            }
            {
                item.attachments && (
                    <ul className="Card-attachments">
                        {
                            item.attachments.map(attach => (
                                <li key={attach.fileId}>
                                    <a href={attach.url} target="_blank" rel="noopener noreferrer">{attach.label}</a>
                                </li>
                            ))
                        }
                    </ul>
                )
            }
            <p className="Card-comment">{item.comment}</p>
        </div>
    );
};

Card.propTypes = propTypes;

export default Card;
