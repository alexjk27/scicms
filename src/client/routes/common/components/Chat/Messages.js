import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import { Panel, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as actions from '../../../../modules/actions';
import Message from './Message';
import MessageInput from './MessageInput';


@connect(state => ({ messages: state.messages, pagination: state.pagination['/messages'] }))
export default class Messages extends Component {
    static propTypes = {
        userId: PropTypes.number,
        messages: PropTypes.object.isRequired,
        pagination: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
    };

    onLoadMessages = () => {
        const { pagination: { path, page, pageSize, hasNext }, userId } = this.props;
        if (hasNext) {
            this.props.dispatch(actions.loadPaginatedPatch(path, page + 1, pageSize, userId));
        }
    };

    onDeleteMessage = (id) => {
        this.props.dispatch(actions.deleteMsg(id));
    };

    renderHeader = () => {
        return (
            <div>Messages</div>
        );
    };

    render() {
        const { messages, userId, pagination: { hasNext } } = this.props;
        return (
            <Panel className="Messages" header={this.renderHeader()} footer={<MessageInput userId={userId} />}>
                <div className="Messages-body" ref={com => (this.scroll = com)}>
                    {
                        hasNext && (
                            <p className="text-center">
                                <Button onClick={this.onLoadMessages}>Load previous</Button>
                            </p>
                        )
                    }
                    {
                        _.map(
                            _.orderBy(messages.items, ['order'], ['asc']),
                            (message, id) => <Message key={id} {...message} onDelete={this.onDeleteMessage} />
                        )
                    }
                </div>
            </Panel>
        );
    }
}
