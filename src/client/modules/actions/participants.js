
export const SET_PARTICIPANT_ENTITY = 'SET_PARTICIPANT_ENTITY';
export function setParticipantEntity(id, name, values) {
    return {
        type: SET_PARTICIPANT_ENTITY,
        isApi: true,
        id,
        name,
        values
    };
}
