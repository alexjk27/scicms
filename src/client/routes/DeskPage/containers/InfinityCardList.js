import React, { PureComponent, PropTypes } from 'react';
import { DropTarget } from 'react-dnd';
import { connect } from 'react-redux';
import ReactList from '../../common/react-list';
import Card from './DraggableCard';


const listTarget = {
    drop(props, monitor) {
        const { desk: { id: deskId }, cards } = props;
        const { userId, id: cardId } = monitor.getItem();

        if (cards.length === 0) {
            props.moveCard(userId, cardId, {
                desk: deskId,
                beforePosition: 0,
                position: 1,
                afterPosition: 2,
            });
        }
    }
};


@DropTarget('card', listTarget, (connectDragSource, monitor) => ({
    connectDropTarget: connectDragSource.dropTarget(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    source: monitor.getItem() || { clientWidth: 0, clientHeight: 0 },
    isOver: monitor.isOver(),
}))
@connect(state => ({ deviceHeight: state.device.height }))
export default class CardList extends PureComponent {
    static propTypes = {
        cards: PropTypes.array.isRequired,
        x: PropTypes.number.isRequired,
        stopScrolling: PropTypes.func,
        source: PropTypes.object.isRequired,
        isOver: PropTypes.bool.isRequired,
        isOverCurrent: PropTypes.bool.isRequired,
        moveCard: PropTypes.func.isRequired,
        deviceHeight: PropTypes.number.isRequired,
        connectDropTarget: PropTypes.func.isRequired,
    };

    state = {
        showPlaceholder: false
    };

    componentWillReceiveProps(nextProps) {
        if (!this.props.isOver && nextProps.isOver) {
            // You can use this as enter handler
            //this.setState({ showPlaceholder: true });
        }

        if (this.props.isOver && !nextProps.isOver) {
            this.setState({ showPlaceholder: false });
        }

        if (this.props.isOverCurrent && !nextProps.isOverCurrent) {
            // You can be more specific and track enter/leave
            // shallowly, not including nested targets
        }
    }

    renderCard = (index, key) => {
        const { x, cards, moveCard } = this.props;
        const item = cards[index];

        return (
            <Card
                x={x} y={index}
                item={item}
                key={key}
                stopScrolling={this.props.stopScrolling}
                moveCard={moveCard}
            />
        );
    };

    render() {
        const { cards, connectDropTarget, source: { clientHeight: height }, deviceHeight } = this.props;
        const { showPlaceholder } = this.state;
        const сardListHeight = deviceHeight - 140;

        return connectDropTarget(
            <div className="CardList desk-items" style={{ overflow: 'auto', height: сardListHeight }}>
                <ReactList
                    itemRenderer={this.renderCard}
                    length={cards.length}
                    pageSize={30}
                    type="variable"
                />
                {
                    cards.length === 0 && showPlaceholder && <div style={{ height }} className="Card placeholder" />
                }
            </div>
        );
    }
}
