export function toCamel(str) {
    return str.replace(/(_[a-z])/g, ($1) => {
        return $1.toUpperCase().replace('_', '');
    });
}

export function toCamelCaseObject(obj) {
    const newObject = {};

    Object.keys(obj).forEach((name) => {
        newObject[toCamel(name)] = obj[name];
    });
    return newObject;
}


export function arrayToCamelCase(response) {
    if (!response) {
        return [];
    }
    return response.map(toCamelCaseObject);
}
