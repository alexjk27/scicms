import React from 'react';
import { FormattedMessage } from 'react-intl';


const validateEmail = (email) => {
  // eslint-disable-next-line max-len, no-useless-escape
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
};
// const validatePassword = (password) => {
//     return password > 7;
// };
// const validatePlainPassword = (password, plainPassword) => {
//     return password === plainPassword;
// };
// const validateNotBlank = (text) => {
//     return text.length;
// };

export const testRequired = (value) => {
    return (value ? undefined : (<FormattedMessage id="validation.required" defaultMessage="Required" />));
};
export const maxLength = max => value => (value && value.length > max ? `Must be ${max} characters or less` : null);
export const number = value => (value && isNaN(Number(value)) ? 'Must be a number' : null);
export const minValue = min => value => (value && value < min ? `Must be at least ${min}` : null);
export const email = value => (value && !validateEmail(value) ? 'Invalid email address' : null);
export const confirmPassword = (value, allValues) => {
    if (value && value !== allValues.password) {
        return 'Password doesn\'t match conformation';
    } else {
        return null;
    }
};

const validate = (values) => {
    const errors = {};

    if (!values.name) {
        errors.name = (<FormattedMessage id="validation.required" defaultMessage="Required" />);
    }

    if (!values.email) {
        errors.email = (<FormattedMessage id="validation.required" defaultMessage="Required" />);
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = (<FormattedMessage id="validation.invalidEmail" defaultMessage="Invalid email address" />);
    }

    return errors;
};


export default validate;
