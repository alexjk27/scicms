import React, { PropTypes } from 'react';
import { Button, ControlLabel, FormGroup, FormControl, HelpBlock, Checkbox, Col, option } from 'react-bootstrap';
import InputElement from 'react-input-mask';
import classnames from 'classnames';
import AutosuggestInput from '../routes/common/components/AutosuggestInput';
import FileInput from '../routes/common/components/FileInput';


export function SubmitButton({ submitting, className, invalid }) {
    return (
        <Button
            type="submit"
            className={classnames('pull-right', className)}
            disabled={submitting || invalid}
            bsStyle="primary"
        >{ submitting && <i className="fa fa-spinner fa-spin" /> } Submit</Button>
    );
}

SubmitButton.propTypes = {
    submitting: PropTypes.bool.isRequired,
    // pristine: PropTypes.bool.isRequired,
    invalid: PropTypes.bool.isRequired,
    className: PropTypes.any,
};


export function renderField(props) {
    const { input, options, required, placeholder, label, help, fieldId, type, meta: { touched, error, warning } } = props;
    let validationState;
    let formControl;

    if (touched && error) {
        validationState = 'error';
    } else if (touched && !error) {
        validationState = 'success';
    } else if (touched && warning) {
        validationState = 'warning';
    } else {
        validationState = null;
    }

    if (type === 'checkbox') {
        return (
            <Checkbox
                {...input}
                validationState={validationState}
            >
                Remember me
            </Checkbox>
        );
    } else if (type === 'phone') {
        formControl = (
            <FormControl
                placeholder={placeholder}
                componentClass={InputElement}
                mask="+7 (999) 999-99-99"
                type={type} {...input}
            />
        );
    } else if (type === 'date') {
        formControl = (
            <FormControl
                placeholder={placeholder}
                type={type}
                {...input}
            />
        );
    } else if (type === 'textarea') {
        formControl = (
            <FormControl
                componentClass={'textarea'}
                placeholder={placeholder}
                type={type} {...input}
            />
        );
    } else if (type === 'autosuggest') {
        formControl = (
            <FormControl
                placeholder={placeholder}
                options={options}
                componentClass={AutosuggestInput}
                type={type}
                {...input}
            />
        );
    } else if (type === 'select') {
        formControl = (
            <FormControl {...input} componentClass="select">
                {
                    options.map(item => (
                        <option key={item.value} value={item.value}>{item.label}</option>
                    ))
                }
            </FormControl>
        );
    } else if (type === 'exFile') {
        formControl = (
            <FormControl
                type={type}
                fieldId={fieldId}
                placeholder={placeholder}
                componentClass={FileInput}
                {...input}
            />
        );
    } else {
        formControl = (
            <FormControl
                type={type}
                {...input}
            />
        );
    }

    if (!label) {
        return (
            <FormGroup validationState={validationState}>
                {formControl}
                {help && <HelpBlock>{help}</HelpBlock>}
                {touched && (error && <HelpBlock>{error}</HelpBlock>)}
                {touched && (warning && <HelpBlock>{warning}</HelpBlock>)}
            </FormGroup>
        );
    }

    return (
        <FormGroup validationState={validationState}>
            <Col componentClass={ControlLabel} sm={4} md={4} lg={4} xs={12}>
                {label} {required && <span>*</span>}
            </Col>
            <Col sm={8} md={8} lg={8} xs={12}>
                {formControl}
                {help && <HelpBlock>{help}</HelpBlock>}
                {touched && (error && <HelpBlock>{error}</HelpBlock>)}
                {touched && (warning && <HelpBlock>{warning}</HelpBlock>)}
            </Col>
        </FormGroup>
    );
}


renderField.propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
    label: PropTypes.string,
    type: PropTypes.string.isRequired,
    options: PropTypes.array,
    placeholder: PropTypes.string,
    help: PropTypes.string,
    fieldId: PropTypes.string,
    required: PropTypes.bool,
};
