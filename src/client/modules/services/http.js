import axios from 'axios';
import { SubmissionError } from 'redux-form';
import Cookies from 'js-cookie';
import config from '../../config';


export const upload = ({ url, files, data, progress, imageTransform }) => (token) => {
    return new Promise((resolve, reject) => {
        const FileAPI = require('fileapi'); // eslint-disable-line global-require
        FileAPI.support.cors = true;

        FileAPI.upload({
            url,
            files: { file: files },
            data,
            progress,
            headers: {
                'X-CSRFToken': Cookies.get('csrftoken'),
                'Authorization': `JWT ${token}`,
            },
            imageTransform,
            complete: (err, xhr) => {
                if ([200, 201].indexOf(xhr.status) > -1) {
                    const response = JSON.parse(xhr.responseText);
                    resolve(response);
                } else {
                    const response = JSON.parse(xhr.responseText);
                    reject(Object.assign({}, response, { httpError: err.message }));
                }
            }
        });
    });
};


export function makeApiInstance(baseURL, token) {
    const headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRFToken': Cookies.get('csrftoken'),
    };

    if (token) {
        headers.Authorization = `JWT ${token}`;
    }

    return axios.create({
        baseURL,
        headers
    });
}

export const get = (url, values) => (token) => {
    const instance = makeApiInstance(config.HOST, token);

    return instance.get(url, { params: values }).then((res) => {
        return res.data;
    }).catch((err) => {
        console.error('GET exception', err);
        throw err;
    });
};

export const submit = (url, values) => (token) => {
    const instance = makeApiInstance(config.HOST, token);

    return instance.post(url, values).then((res) => {
        if (res && [200, 201].indexOf(res.status) > -1) {
            return res.data;
        }
        return null;
    }).catch((error) => {
        if (error.response && [400, 401, 409].indexOf(error.response.status) > -1) {
            throw new SubmissionError(Object.assign({},
                error.response.data,
                { status: error.response.status }));
        } else {
            throw error;
        }
    });
};

export const update = (url, values) => (token) => {
    const instance = makeApiInstance(config.HOST, token);

    return instance.post(url, values).then((res) => {
        if (res && [200, 201].indexOf(res.status) > -1) {
            return res.data;
        }
        return null;
    }).catch(({ response }) => {
        if (response && [400, 401, 409].indexOf(response.status) > -1) {
            throw new SubmissionError(Object.assign({},
                response.data,
                { httpStatus: response.status }));
        } else {
            console.error(response);
            throw new Error(response.data.message);
        }
    });
};


export const deleteItem = (url, jwt) => (token) => {
    let instance = axios;
    if (jwt) {
        instance = makeApiInstance(jwt, config.HOST, token);
    } else {
        return null;
    }

    return instance.delete(url).then((res) => {
        if (res && [200, 201].indexOf(res.status) > -1) {
            return res.data;
        }
        return null;
    }).catch(({ response }) => {
        if (response && [400, 401, 409].indexOf(response.status) > -1) {
            throw new SubmissionError(Object.assign({},
                response.data,
                { httpStatus: response.status }));
        } else {
            console.error(response);
            throw new Error(response.data.message);
        }
    });
};
