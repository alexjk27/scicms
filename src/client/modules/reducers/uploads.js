import * as actionTypes from '../actions';


const initialState = {
    progress: {},
    files: {},
};


export default function uploads(state = initialState, action = {}) {
    switch (action.type) {
        case actionTypes.UPLOADED_FILE_PROGRESS:
            return { ...state, progress: { ...state.progress, [action.field]: action.progress } };
        case actionTypes.UPLOADED_FILES:
            return { ...state, files: { ...state.files, [action.field]: action.file } };
        default:
            return state;
    }
}
